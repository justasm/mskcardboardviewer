Android 3D Musculoskeletal Visualiser for Cardboard
===================================================

![Shoulder kinematics of cricket bowler model by the Musculoskeletal Mechanics Group at Imperial College London.][hero]

Used in the "Visualising Musculoskeletal Models Using Low-Cost Virtual Reality" MEng project. Developed using [Android Studio][1].

Android Projects
----------------
- AndroidGLUT/aglut/
	- OpenGL utilities for Android, including VTK file loading; wrappers for Texture and Program; buffer initialisation; 3D maths, and more.

- CardboardModelViewer/app/
	- Primary component, loads and displays various 3D musculoskeletal models in stereoscopic 3D as designed for mobile VR headsets like Google Cardboard. Contains majority of musculoskeletal model files.

- CardboardModelViewer/remote/
	- A complementary app that can communicate to the primary one (see above) via Bluetooth. This allows additional, remote control of the viewer during operation when the touchscreen is obstructed.

- CardboardModelViewer/common/
	- Several definitions common to both /app and /remote, including the Bluetooth protocol and set of defined musculoskeletal models.

Other
-----
Please direct any questions to jm4711@ic.ac.uk.

[hero]: hero.png
[1]: https://developer.android.com/sdk/index.html