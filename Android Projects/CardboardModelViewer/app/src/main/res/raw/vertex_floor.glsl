uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform mat4 uModelMatrix;

attribute vec4 aPosition;
attribute vec4 aColor;
attribute vec3 aNormal;

varying vec4 vColor;
varying vec3 vGrid;

void main() {
   vec3 modelVertex = vec3(uModelMatrix * aPosition);
   vGrid = modelVertex;
   vColor = aColor;
   gl_Position = uMVPMatrix * aPosition;
}