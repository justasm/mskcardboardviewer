uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform vec3 uLightPosition;
uniform float uTime;
uniform float uAlpha;
uniform vec3 uColorTint;

attribute vec4 aPosition1;
attribute vec4 aPosition2;
attribute vec3 aNormal1;
attribute vec3 aNormal2;
attribute vec4 aColor;

varying vec4 vColor;
varying vec2 vTexCoordinate;

void main() {
    // linear
    vec4 pos = mix(aPosition1, aPosition2, uTime);
    vec3 nor = mix(aNormal1, aNormal2, uTime);

    // get vertex in eye space
    vec3 modelViewPosition = vec3(uMVMatrix * pos);
    // get normal's orientation in eye space
    vec3 modelViewNormal = vec3(uMVMatrix * vec4(nor, 0));

    vec3 lightVector = normalize(uLightPosition - modelViewPosition);

    float diffuse = max(dot(modelViewNormal, lightVector), 0.4);

    vColor = vec4(aColor.rgb * diffuse * uColorTint, uAlpha);

    vec4 sPlane = vec4(1.0/50.0, 0.0, 0.0, 0.0);
    vec4 tPlane = vec4(0.0, 1.0/50.0, 0.0, 0.0);
    vTexCoordinate.s = dot(aPosition1, sPlane);
    vTexCoordinate.t = dot(aPosition1, tPlane);

    gl_Position = uMVPMatrix * pos;

    gl_PointSize = max(2.0, 100.0 / max(5.0, gl_Position.z));
}