precision mediump float;

uniform float uRadius;

varying vec4 vColor;
varying vec3 vGrid;

void main() {
    float dist = length(vGrid.xz);
    float alpha = smoothstep(uRadius, 1.6 * uRadius, dist);
    gl_FragColor = mix(vColor, vec4(0.0, 0.0, 0.0, 1.0), alpha);
}