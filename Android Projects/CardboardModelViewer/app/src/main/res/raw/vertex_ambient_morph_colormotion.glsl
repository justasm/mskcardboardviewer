uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform vec3 uLightPosition;
uniform float uTime;
uniform float uMaxPositionDifferenceSq;
uniform float uAlpha;
uniform vec3 uColorTint;

attribute vec4 aPosition1;
attribute vec4 aPosition2;
attribute vec3 aNormal1;
attribute vec3 aNormal2;
attribute vec4 aColor;

varying vec4 vColor;

void main() {
    // linear
    vec4 pos = mix(aPosition1, aPosition2, uTime);
    vec3 nor = mix(aNormal1, aNormal2, uTime);

    // get vertex in eye space
    vec3 modelViewPosition = vec3(uMVMatrix * pos);
    // get normal's orientation in eye space
    vec3 modelViewNormal = vec3(uMVMatrix * vec4(nor, 0));

    vec3 lightVector = normalize(uLightPosition - modelViewPosition);

    float diffuse = max(dot(modelViewNormal, lightVector), 0.3);

    vec4 d = (aPosition1 - aPosition2);
    float differenceAlpha = dot(d, d) / uMaxPositionDifferenceSq;
    vColor = mix(vec4(aColor.rgb * uColorTint, uAlpha), vec4(1f, 0, 0, uAlpha), differenceAlpha) * diffuse;

    gl_Position = uMVPMatrix * pos;

    gl_PointSize = max(2.0, 100.0 / max(5.0, gl_Position.z));
}