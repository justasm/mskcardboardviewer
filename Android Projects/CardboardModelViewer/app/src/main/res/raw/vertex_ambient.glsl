uniform mat4 uMVPMatrix;
uniform mat4 uMVMatrix;
uniform vec3 uLightPosition;

attribute vec4 aPosition;
attribute vec3 aNormal;
attribute vec4 aColor;

varying vec4 vColor;

void main() {
    // get vertex in eye space
    vec3 modelViewPosition = vec3(uMVMatrix * aPosition);
    // get normal's orientation in eye space
    vec3 modelViewNormal = vec3(uMVMatrix * vec4(aNormal, 0));

    vec3 lightVector = normalize(uLightPosition - modelViewPosition);

    float diffuse = max(dot(modelViewNormal, lightVector), 0.3);

    vColor = aColor * diffuse;
    gl_Position = uMVPMatrix * aPosition;
}