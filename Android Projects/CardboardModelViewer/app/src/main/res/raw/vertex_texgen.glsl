uniform mat4 uMVPMatrix;

attribute vec4 aPosition;
attribute vec4 aColor;
attribute vec2 aTexCoordinate;

varying vec4 vColor;
varying vec2 vTexCoordinate;

void main() {
    vColor = aColor;

    vec4 sPlane = vec4(1.0, 0.0, 0.0, 0.0);
    vec4 tPlane = vec4(0.0, 1.0, 0.0, 0.0);
    vTexCoordinate.s = dot(aPosition, sPlane);
    vTexCoordinate.t = dot(aPosition, tPlane);

    gl_Position = uMVPMatrix * aPosition;
}