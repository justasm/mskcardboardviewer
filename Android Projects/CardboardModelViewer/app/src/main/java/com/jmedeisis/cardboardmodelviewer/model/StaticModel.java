package com.jmedeisis.cardboardmodelviewer.model;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.jmedeisis.aglut.FloatArrayVector3;
import com.jmedeisis.aglut.GLMatrix;
import com.jmedeisis.aglut.NormalGen;
import com.jmedeisis.aglut.RayIntersection;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import static com.jmedeisis.aglut.FloatArrayVector3.length2;
import static com.jmedeisis.aglut.FloatArrayVector3.sub;

/**
 * A static model that configures and holds references to its own IBO and VBOs.
 * The model is automatically centered on the origin in model space if it is not so already.
 * <p>
 * Created by Justas on 25/11/2014.
 */
public class StaticModel {
    private static final int BYTES_PER_SHORT = 2;
    protected static final int BYTES_PER_FLOAT = 4;

    private int indexBufferHandle;
    private int[] positionBufferHandles;
    private int[] normalBufferHandles;
    private int colorBufferHandle;

    private int vertexCount;
    private float maxRange;
    private float rangeX;
    private float[] center = new float[3];

    // shadow copies of GPU data for collision calls
    private ShortBuffer indices;
    private FloatBuffer[] vertices;

    public StaticModel(ShortBuffer indices, FloatBuffer colors, FloatBuffer... vertices){
        this(indices, colors, vertices, null, true);
    }

    /**
     *
     * @param indices cannot be null.
     * @param colors cannot be null.
     * @param vertices must be at least of length 1.
     * @param normals may be null.
     * @param compensateOffset
     */
    public StaticModel(ShortBuffer indices, FloatBuffer colors, FloatBuffer[] vertices, FloatBuffer[] normals, boolean compensateOffset){
        if(vertices.length < 1)
            throw new IllegalArgumentException("A model must have at least one set of vertices.");
        if(null != normals && vertices.length != normals.length)
            throw new IllegalArgumentException("Number of normal buffers must match number of vertex buffers.");

        this.indices = indices;
        this.vertices = vertices;

        // calculate model mean based on first set of vertices
        float minX = Float.MAX_VALUE;
        float minY = Float.MAX_VALUE;
        float minZ = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE;
        float maxY = Float.MIN_VALUE;
        float maxZ = Float.MIN_VALUE;

        float modelMeanX = 0;
        float modelMeanY = 0;
        float modelMeanZ = 0;
        // TODO use meanFrameIndex, instead of 0
        while(vertices[0].remaining() > 0){
            float x = vertices[0].get();
            float y = vertices[0].get();
            float z = vertices[0].get();

            minX = Math.min(minX, x);
            minY = Math.min(minY, y);
            minZ = Math.min(minZ, z);

            maxX = Math.max(maxX, x);
            maxY = Math.max(maxY, y);
            maxZ = Math.max(maxZ, z);

            modelMeanX += x;
            modelMeanY += y;
            modelMeanZ += z;
        }
        vertices[0].position(0);
        modelMeanX /= vertices[0].capacity() / 3;
        modelMeanY /= vertices[0].capacity() / 3;
        modelMeanZ /= vertices[0].capacity() / 3;

        rangeX = Math.abs(maxX - minX);
        float modelRangeY = Math.abs(maxY - minY);
        float modelRangeZ = Math.abs(maxZ - minZ);
        maxRange = Math.max(rangeX, Math.max(modelRangeY, modelRangeZ));

        // compensating for offset, centering model at 0, 0, 0

        int pos;
        for(int i = 0; i < vertices.length; i++){
            if(compensateOffset){
                pos = 0;
                while(vertices[i].remaining() > 0){
                    vertices[i].put(pos++, vertices[i].get() - modelMeanX);
                    vertices[i].put(pos++, vertices[i].get() - modelMeanY);
                    vertices[i].put(pos++, vertices[i].get() - modelMeanZ);
                }
            }
            vertices[i].position(0);
        }

        FloatArrayVector3.set(center, modelMeanX, modelMeanY, modelMeanZ);

        // setup IBO & VBOs
        //                  indices     colors      positions+normals
        int bufferCount =   1 +         1 +         2 * vertices.length;
        final int[] buffers = new int[bufferCount];
        GLES20.glGenBuffers(bufferCount, buffers, 0);

        indexBufferHandle = buffers[0];
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);
        GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, indices.capacity() * BYTES_PER_SHORT,
                indices, GLES20.GL_STATIC_DRAW);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

        colorBufferHandle = buffers[1];
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, colorBufferHandle);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, colors.capacity() * BYTES_PER_FLOAT,
                colors, GLES20.GL_STATIC_DRAW);

        positionBufferHandles = new int[vertices.length];
        normalBufferHandles = new int[vertices.length];
        for(int i = 0; i < vertices.length; i++){
            positionBufferHandles[i] = buffers[2 + i];
            normalBufferHandles[i] = buffers[vertices.length + 2 + i];

            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, positionBufferHandles[i]);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertices[i].capacity() * BYTES_PER_FLOAT,
                    vertices[i], GLES20.GL_STATIC_DRAW);

            FloatBuffer ns;
            if(null == normals || null == normals[i]){
                ns = NormalGen.getNormals(vertices[i], indices);
            } else {
                ns = normals[i];
            }
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, normalBufferHandles[i]);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, ns.capacity() * BYTES_PER_FLOAT,
                    ns, GLES20.GL_STATIC_DRAW);
        }
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        vertexCount = indices.capacity();
    }

    /** @return maximum extent of model in model space, in any one direction. */
    public float getMaxRange(){
        return maxRange;
    }

    public float getMaxRangeX(){
        return rangeX;
    }

    public float[] getCenter(){
        return center;
    }

    public void draw(int shaderPositionHandle, int shaderColorHandle){
        bindPositions(0, shaderPositionHandle);
        bindColors(shaderColorHandle);
        bindIndicesDraw(GLES20.GL_TRIANGLES);
    }

    public void draw(int shaderPositionHandle, int shaderNormalHandle, int shaderColorHandle){
        bindPositions(0, shaderPositionHandle);
        bindNormals(0, shaderNormalHandle);
        bindColors(shaderColorHandle);
        bindIndicesDraw(GLES20.GL_TRIANGLES);
    }

    protected void bindPositions(int target, int shaderPositionHandle){
        if(target >= positionBufferHandles.length)
            throw new IllegalArgumentException("Target " + target + " does not exist.");

        GLES20.glEnableVertexAttribArray(shaderPositionHandle);

        // position
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, positionBufferHandles[target]);
        GLES20.glVertexAttribPointer(shaderPositionHandle, 3, GLES20.GL_FLOAT, false, 0, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    protected void bindNormals(int target, int shaderNormalHandle){
        // we're assuming at least positions have been bound, so don't bother verifying target is valid

        GLES20.glEnableVertexAttribArray(shaderNormalHandle);

        // normal
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, normalBufferHandles[target]);
        GLES20.glVertexAttribPointer(shaderNormalHandle, 3, GLES20.GL_FLOAT, false, 0, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    protected void bindColors(int shaderColorHandle){
        GLES20.glEnableVertexAttribArray(shaderColorHandle);
        // color
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, colorBufferHandle);
        GLES20.glVertexAttribPointer(shaderColorHandle, 4, GLES20.GL_FLOAT, false, 0, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    protected void bindIndicesDraw(int mode){
        // indices
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, indexBufferHandle);

        // OK NOW DRAW!
        GLES20.glDrawElements(mode, vertexCount, GLES20.GL_UNSIGNED_SHORT, 0);

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    /*
    COLLISION
     */
    private float[] inverseModelMatrix = new float[16];
    private float[] rayStartModel = new float[]{0, 0, 0, 1};
    private float[] rayEndModel = new float[]{0, 0, 0, 1};
    private float[] p0 = new float[3];
    private float[] p1 = new float[3];
    private float[] p2 = new float[3];
    private float[] hitDistance = new float[3];
    private float[] hitCandidate = new float[]{0, 0, 0, 1};
    /**
     * @param hit position of collision (closest to ray origin) in world space if intersection is found.
     * @return true if ray intersects target.
     */
    public boolean rayPick(int target, float[] rayStartWorld, float[] rayEndWorld, float[] modelMatrix, float[] hit){
        boolean collision = false;
        float minLength2 = Float.MAX_VALUE;

        // place rayStart / rayEnd in model space coordinates
        Matrix.invertM(inverseModelMatrix, 0, modelMatrix, 0);
        GLMatrix.multiplyMV(rayStartModel, inverseModelMatrix, rayStartWorld);
        GLMatrix.multiplyMV(rayEndModel, inverseModelMatrix, rayEndWorld);

        // iterate over each face, calculate its normal, and store as running sum per member vertex
        indices.position(0);
        while(indices.hasRemaining()){
            short p0i = indices.get();
            short p1i = indices.get();
            short p2i = indices.get();

            // we're assuming vertices is contiguous positions only here
            p0[0] = vertices[target].get(3 * p0i + 0);
            p0[1] = vertices[target].get(3 * p0i + 1);
            p0[2] = vertices[target].get(3 * p0i + 2);

            p1[0] = vertices[target].get(3 * p1i + 0);
            p1[1] = vertices[target].get(3 * p1i + 1);
            p1[2] = vertices[target].get(3 * p1i + 2);

            p2[0] = vertices[target].get(3 * p2i + 0);
            p2[1] = vertices[target].get(3 * p2i + 1);
            p2[2] = vertices[target].get(3 * p2i + 2);

            if(RayIntersection.rayVsTriangle(rayStartModel, rayEndModel, p0, p1, p2, hitCandidate)){
                sub(hitDistance, rayStartModel, hitCandidate);
                float candidateLength2 = length2(hitDistance);
                if(candidateLength2 < minLength2){
                    minLength2 = candidateLength2;
                    System.arraycopy(hitCandidate, 0, hit, 0, hitCandidate.length);
                }
                collision = true;
            }
        }
        indices.position(0);

        if(collision){
            // hit test was performed in model coordinates, convert back to world
            GLMatrix.multiplyMV(hit, modelMatrix, hit);
        }
        return collision;
    }
}
