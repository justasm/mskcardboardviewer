package com.jmedeisis.cardboardmodelviewer.ui;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.google.vrtoolkit.cardboard.CardboardView;
import com.jmedeisis.common.bt.BTConnection;
import com.jmedeisis.common.bt.ControlMessage;
import com.jmedeisis.cardboardmodelviewer.CBModelRenderer;
import com.jmedeisis.cardboardmodelviewer.R;
import com.jmedeisis.common.model.ModelData;

/**
 * Created by Justas on 04/11/2014.
 */
public class GestureCardboardView extends CardboardView {

    private static final String LOG_TAG = GestureCardboardView.class.getSimpleName();
    private CBModelRenderer customRenderer;
    private BTConnection connection;
    private BluetoothAdapter btAdapter;

    public GestureCardboardView(Context context) {
        this(context, null);
    }

    public GestureCardboardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setSettingsButtonEnabled(false);

        final ControlGestureListener gestureListener = new ControlGestureListener();
        final GestureDetector gestureDetector = new GestureDetector(context, gestureListener);
        final ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(context, gestureListener);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                scaleGestureDetector.onTouchEvent(event);
                return gestureDetector.onTouchEvent(event);
            }
        });

        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(btAdapter.isEnabled()){
            initBtServer();
        }
    }

    private void initBtServer(){
        connection = new BTConnection();
        connection.addListener(new Messenger(btHandler));
        connection.listen();

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        getContext().startActivity(discoverableIntent);
    }

    @Override
    public void setRenderer(StereoRenderer renderer){
        super.setRenderer(renderer);

        if(renderer instanceof CBModelRenderer){
            customRenderer = (CBModelRenderer) renderer;
        }
    }

    private class ControlGestureListener extends GestureDetector.SimpleOnGestureListener
            implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener,
            ScaleGestureDetector.OnScaleGestureListener {

        @Override
        public boolean onDown(MotionEvent event){
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent event){
            if(null != customRenderer) customRenderer.onSingleTap();
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent event){
            if(null != customRenderer) customRenderer.onDoubleTap();
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            if(null != customRenderer) customRenderer.onScale(detector.getScaleFactor());
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true; // handle this gesture
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

        }
    }

    private final ControlMessage.ControlReceiver receiver = new ControlMessage.ControlReceiver() {
        @Override
        public void onOrient(float x, float y, float z, float w) {
            if(null != customRenderer) customRenderer.onRemoteOrientation(x, y, z, w);
        }

        @Override
        public void onScale(float scaleFactor) {
            if(null != customRenderer) customRenderer.onScale(scaleFactor);
        }

        @Override
        public void onSetOrigin() {
            if(null != customRenderer) customRenderer.setRotationOrigin();
        }

        @Override
        public void onResetOrigin(){
            if(null != customRenderer) customRenderer.resetRotationOrigin();
        }

        @Override
        public void onToggleAutoAnimation() {
            if(null != customRenderer) customRenderer.toggleAutoAnimation();
        }

        @Override
        public void onAdvanceAnimation(float step) {
            if(null != customRenderer) customRenderer.advanceAnimation(step);
        }

        @Override
        public void onNewPreferences(boolean doubleRotation, boolean texture, boolean colorOverlay,
                                     boolean meanPointCloud, boolean meanSideBySide, boolean debugCube,
                                     boolean detachCamera, boolean morph, boolean allComponents, String navControl) {
            final Context context = getContext();
            SharedPreferences.Editor sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_double_rotation), doubleRotation);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_texture), texture);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_color_overlay), colorOverlay);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_point_cloud_mean), meanPointCloud);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_side_mean), meanSideBySide);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_debug_cube), debugCube);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_detach_camera), detachCamera);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_morph), morph);
            sharedPreferences.putString(context.getString(R.string.pref_key_magnet_tap_control), navControl);
            sharedPreferences.putBoolean(context.getString(R.string.pref_key_altogether), allComponents);
            sharedPreferences.apply();

            if(null != customRenderer) customRenderer.onPreferencesUpdated();
        }

        @Override
        public void onChooseModel(int index) {
            if(null != customRenderer && index >= 0 && index < ModelData.values().length){
                customRenderer.loadModelAsync(ModelData.values()[index]);
            }
        }
    };

    private final Handler btHandler = new Handler() {
        @Override
        public void handleMessage(Message msg){
            switch(msg.what){
                case BTConnection.MESSAGE_STATE_CHANGE:
                    BTConnection.State newState = (BTConnection.State) msg.obj;
                    Log.d(LOG_TAG, "new BT state: " + newState);
                    if(BTConnection.State.NONE == newState){
                        connection.listen();
                    }
                    break;
                case BTConnection.MESSAGE_READ:
                    byte[] data = (byte[]) msg.obj;
                    ControlMessage.decode(data, receiver);
                    break;
            }
        }
    };
}
