package com.jmedeisis.cardboardmodelviewer.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.jmedeisis.cardboardmodelviewer.CBModelRenderer;
import com.jmedeisis.cardboardmodelviewer.R;
import com.jmedeisis.cardboardmodelviewer.ui.widget.CardboardOverlayView;
import com.jmedeisis.common.model.ModelData;
import com.jmedeisis.common.state.DemoState;

public class MainActivity extends CardboardActivity {

    private final static int REQUEST_MODEL_IDX = 1;
    public final static String MODEL_IDX_EXTRA = "model_idx";
    private final static int REQUEST_SETTINGS = 2;

    private static final String EXTRA_DEMO_TYPE = "com.jmedeisis.cardboardmodelviewer.ui.EXTRA_DEMO_TYPE";

    private CBModelRenderer renderer;

    public static Intent createIntent(Context context, DemoState.Type type){
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_DEMO_TYPE, type.toString());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.viewer_preferences, false);

        // volume keys are often accidentally pressed while in Cardboard
        // disable for duration of Activity
        setVolumeKeysMode(VolumeKeys.DISABLED);

        setContentView(R.layout.activity_main);

        CardboardView cardboardView = (CardboardView) findViewById(R.id.cardboard_view);
        CardboardOverlayView overlayView = (CardboardOverlayView) findViewById(R.id.overlay);
        Switch autoAnimationSwitch = (Switch) findViewById(R.id.auto_animation_switch);
        Switch doubleRotationSwitch = (Switch) findViewById(R.id.double_rotation_switch);
        SeekBar timeBar = (SeekBar) findViewById(R.id.time_bar);
        Spinner modeSpinner = (Spinner) findViewById(R.id.model_mode_spinner);
        Button modelButton = (Button) findViewById(R.id.choose_model_button);
        Button sceneButton = (Button) findViewById(R.id.next_scene_button);
        View settingsButton = findViewById(R.id.settings_button);
        View toggleUiButton = findViewById(R.id.toggle_ui_button);

        DemoState.Type demoType = DemoState.Type.DEBUG;
        Bundle extras = getIntent().getExtras();
        if(null != extras && extras.containsKey(EXTRA_DEMO_TYPE)){
            demoType = DemoState.Type.valueOf(extras.getString(EXTRA_DEMO_TYPE));
        }

        renderer = new CBModelRenderer(this, cardboardView, new DemoState(demoType), overlayView,
                modelButton, settingsButton, sceneButton, autoAnimationSwitch, doubleRotationSwitch, timeBar, modeSpinner, toggleUiButton);
        cardboardView.setRenderer(renderer);

        setCardboardView(cardboardView);

        modelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChooseModelActivity.class);
                startActivityForResult(intent, REQUEST_MODEL_IDX);
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConfigureSettingsActivity.class);
                startActivityForResult(intent, REQUEST_SETTINGS);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_MODEL_IDX) {
            if (resultCode == RESULT_OK) {
                int idx = data.getIntExtra(MODEL_IDX_EXTRA, -1);
                if(null != renderer && idx >= 0){
                    renderer.loadModelAsync(ModelData.values()[idx]);
                }
            }
        } else if(requestCode == REQUEST_SETTINGS){
            renderer.onPreferencesUpdated();
        }
    }

    @Override
    public void onCardboardTrigger(){
        if(null != renderer) renderer.onCardboardTrigger();
    }
}
