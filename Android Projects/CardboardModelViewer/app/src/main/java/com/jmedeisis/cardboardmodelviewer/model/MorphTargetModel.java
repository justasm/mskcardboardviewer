package com.jmedeisis.cardboardmodelviewer.model;

import android.opengl.GLES20;

import com.jmedeisis.aglut.FloatArrayVector3;
import com.jmedeisis.aglut.Texture;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * A model capable of interpolating / morphing between multiple keyframes.
 * Call {@link #setPositionProportion(float)} to manually set position.
 * <p>
 * Use {@link #draw(int, int, int, int, int, int, int, int, int)} if animating.
 * <p>
 * Created by Justas on 25/11/2014.
 */
public class MorphTargetModel extends StaticTexturedModel {
    @SuppressWarnings("UnusedDeclaration")
    private static final String LOG_TAG = MorphTargetModel.class.getSimpleName();

    /** 0 < position < frameCount-1 */
    private float position;
    private int currentKeyframe;
    private int nextKeyframe;
    private final int frameCount;
    private boolean forwards;

    private float maxLengthSq;

    /** @see com.jmedeisis.cardboardmodelviewer.model.StaticModel#StaticModel(java.nio.ShortBuffer, java.nio.FloatBuffer, java.nio.FloatBuffer[], java.nio.FloatBuffer[], boolean) */
    public MorphTargetModel(ShortBuffer indices, FloatBuffer colors, Texture texture, FloatBuffer[] vertices, FloatBuffer[] normals, boolean compensateOffset) {
        super(indices, colors, null, texture, vertices, normals, compensateOffset);

        if(vertices.length > 1){
            // Calculate the max length change across 2 key-frames for a single vertex
            // This is used for scaling calculations in the shader.
            maxLengthSq = 0;

            for (FloatBuffer v : vertices) {
                v.position(0);
            }

            float[] previous = new float[3];
            float[] current = new float[3];
            float[] result = new float[3];
            while(vertices[0].remaining() > 0){
                vertices[0].get(previous);
                for(int i = 1; i < vertices.length; i++){
                    vertices[i].get(current);

                    FloatArrayVector3.sub(result, current, previous);
                    float lengthSq = FloatArrayVector3.length2(result);
                    if(lengthSq > maxLengthSq){
                        maxLengthSq = lengthSq;
                    }

                    System.arraycopy(current, 0, previous, 0, current.length);
                }
            }
        }

        currentKeyframe = 0;
        nextKeyframe = 0;
        position = 0;
        forwards = true;
        frameCount = vertices.length;
    }

    /**
     * @param proportion at 0, model is at keyframe 0; at 1, model is at keyframe frameCount-1.
     */
    public void setPositionProportion(float proportion){
        if(frameCount < 2) return; // don't animate single-frame models
        setPosition(proportion * (frameCount - 1));
    }

    public float getPositionProportion(){
        return position / (frameCount - 1);
    }

    public void advancePosition(float step){
        setPosition(Math.max(0, Math.min(position + step, frameCount - 1)));
    }

    /** Watch out! Does no bound checks on newPosition. */
    private void setPosition(float newPosition){
        position = newPosition;

        if(forwards && newPosition == frameCount - 1){
            forwards = false;
        } else if(!forwards && newPosition == 0){
            forwards = true;
        }

        if(forwards){
            currentKeyframe = (int) position; // floor it
            nextKeyframe = Math.min(currentKeyframe + 1, frameCount - 1);
        } else {
            currentKeyframe = (int) Math.min(position + 1, frameCount - 1); // ceil it; case where proportion=1, min it
            nextKeyframe = Math.max(currentKeyframe - 1, 0);
        }
    }

    private float getAlpha(){
        return Math.abs(position - currentKeyframe);
    }

    public void draw(int mode, int shaderTimeHandle,
                     int shaderPositionHandle1, int shaderPositionHandle2,
                     int shaderNormalHandle1, int shaderNormalHandle2, int shaderColorHandle,
                     int shaderMaxDifferenceHandle, int shaderTextureUniformHandle){
        // time uniform
        GLES20.glUniform1f(shaderTimeHandle, getAlpha());

        // max difference uniform
        GLES20.glUniform1f(shaderMaxDifferenceHandle, maxLengthSq);

        bindPositions(currentKeyframe, shaderPositionHandle1);
        bindNormals(currentKeyframe, shaderNormalHandle1);
        bindPositions(nextKeyframe, shaderPositionHandle2);
        bindNormals(nextKeyframe, shaderNormalHandle2);
        bindColors(shaderColorHandle);
        bindTexture(shaderTextureUniformHandle);
        bindIndicesDraw(mode);
    }

}
