package com.jmedeisis.cardboardmodelviewer.model;

import android.content.Context;

import com.jmedeisis.aglut.GLMatrix;
import com.jmedeisis.cardboardmodelviewer.data.CsvReader;
import com.jmedeisis.common.model.ModelData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a frame-based transformation.
 *
 * Created by Justas on 22/04/2015.
 */
public class KeyframeTransformation {

    public List<float[]> rotationMatrices;
    private float frame = 0;

    public KeyframeTransformation(Context context, ModelData.Transformation transformation) throws IOException {
        if(null == transformation){
            rotationMatrices = new ArrayList<>(1);
            float[] identity = new float[16];
            GLMatrix.setIdentity(identity);
            rotationMatrices.add(identity);
        } else {
            rotationMatrices = CsvReader.readRotationMatricesFromCsv(context, transformation.rotationMatrixPath);
        }
    }

    public void setPositionProportion(float proportion){
        setFrame(proportion * (rotationMatrices.size() - 1));
    }

    public void advancePosition(float step){
        setFrame(Math.max(0, Math.min(frame + step, rotationMatrices.size() - 1)));
    }

    private void setFrame(float frame){
        this.frame = frame;
    }

    public float[] get(){
        return rotationMatrices.get((int) frame);
    }
}
