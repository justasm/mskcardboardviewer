package com.jmedeisis.cardboardmodelviewer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;
import com.jmedeisis.aglut.ColorGen;
import com.jmedeisis.aglut.FloatArrayVector3;
import com.jmedeisis.aglut.GLMatrix;
import com.jmedeisis.aglut.Program;
import com.jmedeisis.aglut.Texture;
import com.jmedeisis.aglut.VTKLoader;
import com.jmedeisis.cardboardmodelviewer.data.ColorMap;
import com.jmedeisis.cardboardmodelviewer.data.CsvReader;
import com.jmedeisis.cardboardmodelviewer.data.WorldLayoutData;
import com.jmedeisis.cardboardmodelviewer.model.KeyframeLine;
import com.jmedeisis.cardboardmodelviewer.model.KeyframeTransformation;
import com.jmedeisis.cardboardmodelviewer.model.MorphTargetModel;
import com.jmedeisis.cardboardmodelviewer.model.StaticModel;
import com.jmedeisis.cardboardmodelviewer.model.StaticTexturedModel;
import com.jmedeisis.cardboardmodelviewer.ui.widget.CardboardOverlayView;
import com.jmedeisis.common.bt.ControlMessage;
import com.jmedeisis.common.model.ModelData;
import com.jmedeisis.common.state.DemoState;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;

import static com.jmedeisis.aglut.FloatArrayVector3.add;
import static com.jmedeisis.aglut.FloatArrayVector3.sub;

/**
 * Currently handles and configures all draw state, executes draw calls.
 * <p>
 * Created by Justas on 19/10/2014.
 */
public class CBModelRenderer implements CardboardView.StereoRenderer {
    private static final String LOG_TAG = "CBModelRenderer";
    private static final int BYTES_PER_SHORT = 2;
    private static final int BYTES_PER_FLOAT = 4;
    private static final float RAD_TO_DEG = 180f / (float)Math.PI;

    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 2000.0f;

    private static final int MS_PER_ALL_KEYFRAMES = 3000;

    private final Activity context;
    private final CardboardView cardboardView;
    private final CardboardOverlayView overlayView;
    private final Button modelButton;
    private final SeekBar timeBar;
    private Switch autoAnimationSwitch;
    private Switch doubleRotationSwitch;
    private boolean enableAnimation;
    private boolean autoAnimating;
    private boolean autoAnimatingDirectionForwards;
    private boolean doubleRotationSpeed;
    private boolean displayTexture;
    private boolean displayColorOverlay;
    private boolean displayPointCloudMean;
    private boolean displaySideBySideMean;
    private boolean displayAllComponents;
    private boolean displayDebugCube;
    private boolean detachedCamera;
    private final Spinner modeSpinner;
    private final View sceneButton;
    private final View settingsButton;
    private final Vibrator vibrator;
    private long autoAnimationStartTime;
    private enum ControlType {
        NAVIGATION,
        MARKERS,
        ANIMATION
    }
    private ControlType controlType;

    public CBModelRenderer(Activity context, CardboardView cardboardView, final DemoState demoState,
                           CardboardOverlayView overlayView, final Button modelButton, final View settingsButton, final Button sceneButton,
                           final Switch autoAnimationSwitch, Switch doubleRotationSwitch,
                           final SeekBar timeBar, final Spinner modeSpinner, View toggleUiButton){
        this.context = context;
        this.cardboardView = cardboardView;
        this.overlayView = overlayView;
        overlayView.setImage(context.getResources().getDrawable(R.drawable.loader_animation));
        overlayView.hideImage();

        this.modelButton = modelButton;
        this.sceneButton = sceneButton;
        this.settingsButton = settingsButton;
        this.timeBar = timeBar;
        this.modeSpinner = modeSpinner;
        modeSpinner.setVisibility(View.INVISIBLE);

        autoAnimating = autoAnimationSwitch.isChecked();
        this.autoAnimationSwitch = autoAnimationSwitch;
        autoAnimationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                autoAnimating = isChecked;
                if(autoAnimating){
                    setAutoAnimationStartTime(CBModelRenderer.this.timeBar.getProgress() / 100f);
                }
            }
        });

        this.doubleRotationSwitch = doubleRotationSwitch;
        doubleRotationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                doubleRotationSpeed = isChecked;
            }
        });

        // TODO DEBUG
        sceneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(modelSetup){
                    if(null != activeBoneModelData.scenes){
                        CBModelRenderer.this.timeBar.setProgress(0);
                        activeSceneIndex = (activeSceneIndex + 1) % activeBoneModelData.scenes.length;
                    }
                }
            }
        });

        onPreferencesUpdated();

        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        setDemoState(demoState);

        toggleUiButton.setOnClickListener(new View.OnClickListener() {
            private boolean visible = true;
            @Override
            public void onClick(View v) {
                if(visible){
                    sceneButton.setVisibility(View.GONE);
                    modelButton.setVisibility(View.GONE);
                    settingsButton.setVisibility(View.GONE);

                    autoAnimationSwitch.setVisibility(View.GONE);
                    timeBar.setVisibility(View.GONE);
                    modeSpinner.setVisibility(View.GONE);

                    visible = false;
                } else {
                    setDemoState(demoState);

                    autoAnimationSwitch.setVisibility(enableAnimation | displayPointCloudMean ? View.VISIBLE : View.INVISIBLE);
                    timeBar.setVisibility(enableAnimation | displayPointCloudMean ? View.VISIBLE : View.INVISIBLE);
                    if(modelSetup){
                        updateModeSpinner(activeBoneModelData);
                    }

                    visible = true;
                }
            }
        });
    }

    public void setDemoState(DemoState demoState){
        demoState.apply(context, internalCommandAdapter);
        int visibility = DemoState.Type.DEBUG == demoState.type ? View.VISIBLE : View.GONE;
        modelButton.setVisibility(visibility);
        settingsButton.setVisibility(visibility);
        sceneButton.setVisibility(visibility);
    }

    public void onPreferencesUpdated(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        doubleRotationSpeed = sharedPreferences.getBoolean(context.getString(R.string.pref_key_double_rotation), false);
        displayTexture = sharedPreferences.getBoolean(context.getString(R.string.pref_key_texture), true);
        displayColorOverlay = sharedPreferences.getBoolean(context.getString(R.string.pref_key_color_overlay), false);
        displayPointCloudMean = sharedPreferences.getBoolean(context.getString(R.string.pref_key_point_cloud_mean), false);
        displaySideBySideMean = sharedPreferences.getBoolean(context.getString(R.string.pref_key_side_mean), false);
        displayAllComponents = sharedPreferences.getBoolean(context.getString(R.string.pref_key_altogether), false);
        displayDebugCube = sharedPreferences.getBoolean(context.getString(R.string.pref_key_debug_cube), true);
        detachedCamera = sharedPreferences.getBoolean(context.getString(R.string.pref_key_detach_camera), false);
        enableAnimation = sharedPreferences.getBoolean(context.getString(R.string.pref_key_morph), false);

        final String navControl = context.getString(R.string.pref_magnet_tap_control_navigation);
        final String control = sharedPreferences.getString(context.getString(R.string.pref_key_magnet_tap_control),
                navControl);

        updateUiAfterPreferenceUpdate(control);
    }

    private void updateUiAfterPreferenceUpdate(String control){
        final String navControl = context.getString(R.string.pref_magnet_tap_control_navigation);
        final String markerControl = context.getString(R.string.pref_magnet_tap_control_markers);
        final String animationControl = context.getString(R.string.pref_magnet_tap_control_animation);

        if(navControl.equals(control)){
            controlType = ControlType.NAVIGATION;
        } else if(markerControl.equals(control)){
            controlType = ControlType.MARKERS;
        } else if(animationControl.equals(control)){
            controlType = ControlType.ANIMATION;
        } else {
            Log.e(LOG_TAG, "Unknown control type " + control);
            controlType = ControlType.NAVIGATION;
        }

        autoAnimationSwitch.setVisibility(enableAnimation | displayPointCloudMean ? View.VISIBLE : View.INVISIBLE);
        timeBar.setVisibility(enableAnimation | displayPointCloudMean ? View.VISIBLE : View.INVISIBLE);
        if(modelSetup){
            updateModeSpinner(activeBoneModelData);
        }
        if(autoAnimating && !enableAnimation) autoAnimationSwitch.toggle();

        doubleRotationSwitch.setVisibility(detachedCamera ? View.VISIBLE : View.INVISIBLE);
        if(doubleRotationSpeed != doubleRotationSwitch.isChecked()) doubleRotationSwitch.toggle();
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        Log.d(LOG_TAG, "viewport width " + width + " height " + height);
        // viewport & projection handled by Cardboard;
        // EyeTransform#getPerspective() is equivalent to the projection matrix
    }

    private Program modelProgram;
    private Program morphModelProgram;
    private Program morphModelProgramTexture;
    private Program morphModelProgramColor;
    private Program morphModelProgramTextureColor;
    private Program debugUiProgram;
    private Program floorProgram;

    private Texture roughTexture;
    private Texture markerTexture;

    private int positionHandle;
    private int colorHandle;
    private int textureCoordHandle;

    private int mvHandle;
    private int mvpHandle;
    private int textureHandle;

    private int positionHandleFloor;
    private int colorHandleFloor;
    private int radiusHandleFloor;
    private int mvpHandleFloor;
    private int mvHandleFloor;
    private int mHandleFloor;

    private StaticTexturedModel marker;
    private StaticModel floor;
    private StaticTexturedModel cube;
    private KeyframeTransformation[] boneModelTransformations;
    private KeyframeTransformation[] sceneTransformations;
    private MorphTargetModel[] boneModel;
    private KeyframeLine[] muscleLines;
    private int activeComponentIndex;
    private ModelData activeBoneModelData;
    private int activeSceneIndex;

    private static final int MIN_HEAD_DISTANCE_FROM_ORIGIN = 10;
    private static final int MAX_HEAD_DISTANCE_FROM_ORIGIN = (int) (4 * FAR_PLANE / 5);
    private float headDistanceFromOrigin;
    private float minRange;
    private float maxRange = 100;

    private boolean modelLoading = false;
    private boolean modelSetup = false;

    /** Center of head rotation. */
    private float[] origin = new float[]{0, 0, 0};

    private float[] modelMatrix = new float[16];
    private float[] modelMatrixDebug = new float[16];
    private float[] modelMatrixFloor = new float[16];
    private float[] modelMatrixCubeRef = new float[16];
    private float[] modelMatrixMarker = new float[16];
    private float[] detachedCameraMatrix = new float[16];
    private float[] viewMatrix = new float[16];
    private float[] modelViewMatrix = new float[16];
    private float[] modelViewProjectionMatrix = new float[16];
    private float[] eyeViewCorrection = new float[16];

    private float[] lightModelMatrix = new float[16];
    private float[] lightPositionModel = new float[]{0, 0, 0, 0};
    private float[] lightPositionWorld = new float[4];
    private float[] lightPositionEye = new float[4];

    private final List<float[]> markers = new ArrayList<>();

    @Override
    public void onSurfaceCreated(EGLConfig eglConfig) {
        GLES20.glClearColor(1, 0, 1, 1);

        checkGLExtensions();
        checkGLInteger("Max point size", GLES20.GL_ALIASED_POINT_SIZE_RANGE);

        // enable culling to remove back faces
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        // enable depth testing
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        //                                         eye xyz,      look xyz,      up xyz
        Matrix.setLookAtM(detachedCameraMatrix, 0, 0f, 0f, 5.0f, 0f, 0f, -5.0f, 0f, 1f, 0f);

        // load our texture
        roughTexture = new Texture(context, "texture/bone.jpg");
        roughTexture.setWrap(Texture.TextureWrap.REPEAT, Texture.TextureWrap.REPEAT);
        roughTexture.enableMipmaps();
        roughTexture.setFiltering(Texture.TextureFilter.LINEAR, Texture.TextureFilter.LINEAR);

        markerTexture = new Texture(context, "texture/marker.png");
        markerTexture.enableMipmaps();
        markerTexture.setFiltering(Texture.TextureFilter.LINEAR, Texture.TextureFilter.LINEAR);

        floor = new StaticModel(
                getAsShortBuffer(WorldLayoutData.faceIndices),
                ColorGen.getColors(4, 0.1f, 0.1f, 0.1f, 1f),
                getAsFloatBuffer(WorldLayoutData.floorPositionData));

        marker = new StaticTexturedModel(
                getAsShortBuffer(WorldLayoutData.faceIndices),
                ColorGen.getColors(4, 1f, 0, 1f, 0.8f),
                getAsFloatBuffer(WorldLayoutData.faceTextureDataIndexed),
                markerTexture,
                getAsFloatBuffer(WorldLayoutData.glyphPositionDataIndexed));

        cube = new StaticTexturedModel(
                getAsShortBuffer(WorldLayoutData.cubeIndices),
                getAsFloatBuffer(WorldLayoutData.cubeColorDataIndexed),
                getAsFloatBuffer(WorldLayoutData.cubeTextureDataIndexed),
                roughTexture,
                getAsFloatBuffer(WorldLayoutData.cubePositionDataIndexed));

        checkGLError("Floor and debug cube created.");

        // replace w/ ambient_vertex if lighting is desired on debug elements
        debugUiProgram = new Program(context, R.raw.vertex_texture, R.raw.fragment_texture);
        positionHandle = debugUiProgram.getHandle("aPosition");
        colorHandle = debugUiProgram.getHandle("aColor");
        textureCoordHandle = debugUiProgram.getHandle("aTexCoordinate");

        mvHandle = debugUiProgram.getHandle("uMVMatrix");
        mvpHandle = debugUiProgram.getHandle("uMVPMatrix");
        textureHandle = debugUiProgram.getHandle("uTexture");


        floorProgram = new Program(context, R.raw.vertex_floor, R.raw.fragment_floor);
        positionHandleFloor = floorProgram.getHandle("aPosition");
        colorHandleFloor = floorProgram.getHandle("aColor");
        radiusHandleFloor = floorProgram.getHandle("uRadius");
        mHandleFloor = floorProgram.getHandle("uModelMatrix");
        mvHandleFloor = floorProgram.getHandle("uMVMatrix");
        mvpHandleFloor = floorProgram.getHandle("uMVPMatrix");

        morphModelProgram = new Program(context, R.raw.vertex_ambient_morph, R.raw.fragment_simple);
        morphModelProgramColor = new Program(context, R.raw.vertex_ambient_morph_colormotion, R.raw.fragment_simple);
        morphModelProgramTexture = new Program(context, R.raw.vertex_ambient_morph_texgen, R.raw.fragment_texture);
        morphModelProgramTextureColor = new Program(context, R.raw.vertex_ambient_morph_colormotion_texgen, R.raw.fragment_texture);

        checkGLError("All programs created.");
    }

    public void loadModelAsync(final ModelData modelData){
        if(modelLoading) return;

        modelLoading = true;
        modelSetup = false;
        resetRotationOrigin();
        // publish update before we start
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                modelButton.setEnabled(false);
                overlayView.showPersistentToast("Loading model..");
                overlayView.showImage();
            }
        });

        new Thread(){
            public void run() {
                try {
                    final long startTime = System.nanoTime();

                    final int componentCount = modelData.components.length;
                    boneModel = new MorphTargetModel[componentCount];
                    boneModelTransformations = new KeyframeTransformation[componentCount];

                    Buffer[] keyframeData;
                    final ShortBuffer[] indices = new ShortBuffer[componentCount];
                    final FloatBuffer[][] vertices = new FloatBuffer[componentCount][];
                    final FloatBuffer[][] normals = new FloatBuffer[componentCount][];
                    for(int i = 0; i < componentCount; i++){
                        // publish update
                        final int current = i + 1;
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                overlayView.showPersistentToast("Loading model.. Decoding " + current + " / " + componentCount);
                            }
                        });

                        // decode each keyframe
                        final int keyframeCount = modelData.components[i].getFrameCount();
                        vertices[i] = new FloatBuffer[keyframeCount];
                        normals[i] = new FloatBuffer[keyframeCount];
                        for(int j = 0; j < keyframeCount; j++){
                            keyframeData = VTKLoader.load(context, modelData.components[i].getAssetPath(j));
                            vertices[i][j] = (FloatBuffer) keyframeData[0];

                            // Note - only load indices once, assumed same for all frames in component
                            // due to pre-computed point correspondence
                            if(null == indices[i]) indices[i] = (ShortBuffer) keyframeData[1];

                            if(keyframeData.length > 2){
                                normals[i][j] = (FloatBuffer) keyframeData[2];
                            }
                        }

                        boneModelTransformations[i] = new KeyframeTransformation(context,
                                modelData.components[i].transformation);
                    }

                    if(null != modelData.scenes){
                        sceneTransformations = new KeyframeTransformation[modelData.scenes.length];
                        for(int i = 0; i < sceneTransformations.length; i++){
                            sceneTransformations[i] = new KeyframeTransformation(context, modelData.scenes[i].transformation);
                        }
                    }

                    if(null != modelData.attachment){
                        final int numFrames = 63;
                        List<float[]> activationValues = CsvReader.readMuscleActivationsFromCsv(context,
                                modelData.attachment.activationAssetPath, numFrames);

                        final int muscleCount = 91;
                        while(activationValues.size() < muscleCount) {
                            float[] missingMuscleActivationValues = new float[numFrames];
                            Arrays.fill(missingMuscleActivationValues, 1f);
                            activationValues.add(missingMuscleActivationValues);
                        }

                        muscleLines = new KeyframeLine[muscleCount];
                        List<List<float[]>> muscleLinesList = CsvReader.readMuscleLinesFromCsvs(context,
                                modelData.attachment.assetPathFormat, muscleCount);
                        for(int i = 0; i < muscleCount; i++){
                            muscleLines[i] = new KeyframeLine(muscleLinesList.get(i), activationValues.get(i));
                        }
                    }

                    final long decodeFinishedTime = System.nanoTime();

                    final FloatBuffer colors = ColorGen.getColors(indices[0].capacity(), 1f, 1f, 1f, 1f);

                    // MorphTargetModels must be created on GL thread
                    cardboardView.queueEvent(new Runnable() {
                        @Override
                        public void run() {
                            // publish update
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    overlayView.showPersistentToast("Loading model.. Uploading");
                                }
                            });

                            // init MorphTargetModels, i.e. upload buffers to GPU
                            for(int i = 0; i < componentCount; i++){
                                // TODO be smarter than just use displayAllComponents here
                                boneModel[i] = new MorphTargetModel(indices[i], colors, roughTexture, vertices[i], normals[i], !displayAllComponents);
                            }
                            activeComponentIndex = 0;
                            activeBoneModelData = modelData;
                            maxRange = boneModel[0].getMaxRange();
                            Log.d(LOG_TAG, "Max model range: " + maxRange);
                            maxRange = Math.min(maxRange, FAR_PLANE / 4);
                            minRange = Math.max(0, Math.min(MIN_HEAD_DISTANCE_FROM_ORIGIN, maxRange / 100f));
                            headDistanceFromOrigin = clamp(maxRange, minRange, MAX_HEAD_DISTANCE_FROM_ORIGIN);
                            setAnimationToMeanFrame();
                            modelSetup = true;
                            modelLoading = false;

                            long uploadFinishedTime = System.nanoTime();
                            DecimalFormat df = new DecimalFormat("#.##");
                            final String decodeTime = df.format((decodeFinishedTime - startTime) / 1000000000.0);
                            final String uploadTime = df.format((uploadFinishedTime - decodeFinishedTime) / 1000000000.0);
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setAutoAnimation(false);
                                    modelButton.setEnabled(true);
                                    updateModeSpinner(modelData);
                                    overlayView.hideImage();
                                    overlayView.show3DToast("Model loaded.\nDecoding took " + decodeTime + "s\nUploading took " + uploadTime + "s");
                                }
                            });
                        }
                    });
                } catch(IOException e){
                    Log.e(LOG_TAG, "Failed to decode model.", e);
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            modelButton.setEnabled(true);
                            overlayView.hideImage();
                            overlayView.show3DToast("Model failed to load due to error.");
                        }
                    });
                }
            }
        }.start();
    }

    private void setAnimationToMeanFrame(){
        timeBar.setProgress(
                (int)(((float)activeBoneModelData.components[activeComponentIndex].meanFrameIndex /
                        (activeBoneModelData.components[activeComponentIndex].getFrameCount() - 1))
                        * 100f));
    }

    private void setActiveModelToMeanFrame(){
        float morphProportion = ((float)activeBoneModelData.components[activeComponentIndex].meanFrameIndex /
                (activeBoneModelData.components[activeComponentIndex].getFrameCount() - 1));
        boneModel[activeComponentIndex].setPositionProportion(morphProportion);
    }

    private void updateModeSpinner(ModelData model){
        if(!enableAnimation && !displayColorOverlay && !displayPointCloudMean){
            modeSpinner.setVisibility(View.INVISIBLE);
            return;
        }

        ArrayAdapter<String> componentAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item);
        componentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        StringBuilder componentLengthTest = new StringBuilder();
        for(int i = 0; i < model.components.length; i++){
            componentAdapter.add(model.components[i].shortLabel);
            componentLengthTest.append(model.components[i].shortLabel);
        }
        modeSpinner.setAdapter(componentAdapter);
        modeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                activeComponentIndex = position;
                if(!autoAnimating) setAnimationToMeanFrame();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        modeSpinner.setVisibility(componentLengthTest.length() > 0 ? View.VISIBLE : View.INVISIBLE);
    }

    private void setBlendingToDefault(){
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    private void setDepthTestToDefault(){
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
    }

    float[] tempHeadDirVector = new float[4];
    float[] headDirVector = new float[4];
    float[] headQuaternion = new float[4];
    float[] headRotationMatrix = new float[16];
    @Override
    public void onNewFrame(HeadTransform headTransform) {
        GLES20.glClearColor(0, 0, 0, 1);

        setBlendingToDefault();
        setDepthTestToDefault();

        headTransform.getQuaternion(headQuaternion, 0); // x, y, z, w

        if(displayDebugCube || detachedCamera) {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    overlayView.show3DToast("qa " + (2.0 * Math.acos(headQuaternion[3]) * RAD_TO_DEG));
                }
            });
        }

        headDistanceFromOrigin = clamp(headDistanceFromOrigin, minRange, MAX_HEAD_DISTANCE_FROM_ORIGIN);

        // distance increased to more easily observe virtual "real" camera
        Matrix.setLookAtM(detachedCameraMatrix, 0, 0f, 1.8f*headDistanceFromOrigin, 0f, 0f, 0f, 0f, 0f, 0f, -1f);

        // floor
        GLMatrix.setIdentity(modelMatrixFloor);
        Matrix.translateM(modelMatrixFloor, 0, 0, -maxRange / 2 - 40f, 0); // below model
        Matrix.scaleM(modelMatrixFloor, 0, FAR_PLANE / 2, 1, FAR_PLANE / 2);

        // bone model transform
        GLMatrix.setIdentity(modelMatrix);
        if(remoteRotationPresent){
            setMatrixToOrientation(modelMatrix, remoteRotationQuaternion, 1);
        }

        // cube always facing camera / eyes
        setMatrixToOrientation(headRotationMatrix, headQuaternion, getCurrentRotationMultiplier());

        tempHeadDirVector[0] = 0;
        tempHeadDirVector[1] = 0;
        tempHeadDirVector[2] = -1;
        tempHeadDirVector[3] = 0;
        GLMatrix.multiplyMV(headDirVector, headRotationMatrix, tempHeadDirVector);

        if(doubleRotationSpeed){
            // if we are treating our head transform as different from what it is, we have
            // to sync the eye view (in #onDrawEye) as it corresponds to the original transform
            setMatrixToOrientation(eyeViewCorrection, headQuaternion, -1);

//            final float yaw = Quaternion.getYawRad(headQuaternion) * RAD_TO_DEG;
//            final float pitch = Quaternion.getPitchRad(headQuaternion) * RAD_TO_DEG;
//            final float roll = Quaternion.getRollRad(headQuaternion) * RAD_TO_DEG;
//            context.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    overlayView.show3DToast("quat yaw " + yaw +
//                            "\n  pitch " + pitch +
//                            "\n   roll " + roll);
//                }
//            });
        }

        // reference - rotating cube @ origin
        float perTimeSpan = 1 / 10000.0f;
        long time = System.currentTimeMillis() % 10000L;
        float refScale = 1f;
        GLMatrix.setIdentity(modelMatrixCubeRef);
        Matrix.scaleM(modelMatrixCubeRef, 0, refScale, refScale, refScale);
        Matrix.rotateM(modelMatrixCubeRef, 0, 360.0f * perTimeSpan * time, 0.5f, 0.5f, 1.0f);

        // light
        GLMatrix.setIdentity(lightModelMatrix);
        Matrix.translateM(lightModelMatrix, 0, 0.0f, 0.0f, -2.0f);
        GLMatrix.multiplyMV(lightPositionWorld, lightModelMatrix, lightPositionModel);

        // TODO DEBUG
        // bone model transform
        /*GLMatrix.setIdentity(modelMatrix);
        float[] demoQuat = new float[4];
        Matrix.setRotateM(modelMatrix, 0,
                (float) (2.0 * Math.acos(demoQuat[3]) * RAD_TO_DEG),
                demoQuat[0], demoQuat[1], demoQuat[2]);*/

        // morph time
        if(modelSetup) {
            if (autoAnimating) {
                int progressInt = (int) (updateAndGetAutoAnimationFraction() * 100);
                if(null != activeBoneModelData.scenes){
                    if(autoAnimatingDirectionForwards && activeSceneIndex < activeBoneModelData.scenes.length - 1 && 99 <= progressInt){
                        ++activeSceneIndex;
                        progressInt = 0;
                        autoAnimationStartTime = System.currentTimeMillis();
                    } else if(!autoAnimatingDirectionForwards && activeSceneIndex > 0 && 0 == progressInt){
                        --activeSceneIndex;
                        progressInt = 100;
                        autoAnimationStartTime = System.currentTimeMillis() - MS_PER_ALL_KEYFRAMES;
                    }
                }
                timeBar.setProgress(progressInt); // seems to be OK to call from non-UI thread?
            }

            float progress = timeBar.getProgress() / 100f;
            // TODO DEBUG as breaks non-scene-based models
            if(!displayAllComponents || activeSceneIndex == 1) boneModel[activeComponentIndex].setPositionProportion(progress);

            // TODO debug usecases where this is applicable
            if(displayAllComponents){
                for(int i = 0; i < activeBoneModelData.components.length; i++) {
                    boneModelTransformations[i].setPositionProportion(progress);
                }
                if(null != activeBoneModelData.scenes) {
                    for (int i = 0; i < activeBoneModelData.scenes.length; i++) {
                        sceneTransformations[i].setPositionProportion(progress);
                    }
                }
            }
            if(null != muscleLines){
                for (KeyframeLine muscleLine : muscleLines) {
                    muscleLine.setPositionProportion(progress);
                }
            }
        }
    }

    float[] eyePerspective;
    @Override
    public void onDrawEye(Eye eye) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

        eyePerspective = eye.getPerspective(NEAR_PLANE, FAR_PLANE);

        // camera is assumed to be identity (i.e. view == eye), hence line below is commented
//        Matrix.multiplyMM(viewMatrix, 0, eyeTransform.getEyeView(), 0, cameraMatrix, 0);

        if(doubleRotationSpeed){
            GLMatrix.multiplyMM(viewMatrix, eyeViewCorrection, eye.getEyeView());
        } else {
            GLMatrix.cpy(viewMatrix, eye.getEyeView());
        }

        Matrix.translateM(viewMatrix, 0,
                origin[0] + headDirVector[0] * headDistanceFromOrigin,
                origin[1] + headDirVector[1] * headDistanceFromOrigin,
                origin[2] + headDirVector[2] * headDistanceFromOrigin);

        if(detachedCamera) {
            // skip eye transform
            GLMatrix.cpy(viewMatrix, detachedCameraMatrix);
        }

        // light per eye
        GLMatrix.multiplyMV(lightPositionEye, viewMatrix, lightPositionWorld);

        /*
        RENDER FLOOR
         */
        // floor
        floorProgram.use();
        drawFloor(eyePerspective);

        /*
        RENDER DEBUG CUBES
         */
        if(displayDebugCube) {
            debugUiProgram.use();

            drawCube(modelMatrixCubeRef, eyePerspective);

            /*
            RENDER DEBUG
             */
            final float AXIS_SIZE = 4;
            drawLine(new float[]{0, 0, 0}, new float[]{AXIS_SIZE, 0, 0}, new float[]{1, 0, 0, 1}, eyePerspective);
            drawLine(new float[]{0, 0, 0}, new float[]{0, AXIS_SIZE, 0}, new float[]{0, 1, 0, 1}, eyePerspective);
            drawLine(new float[]{0, 0, 0}, new float[]{0, 0, AXIS_SIZE}, new float[]{0, 0, 1, 1}, eyePerspective);

            final float mult = (float)(2 * AXIS_SIZE / Math.sin(Math.acos(headQuaternion[3])));
            drawLine(
                    new float[]{-mult * headQuaternion[0], -mult * headQuaternion[1], -mult * headQuaternion[2]},
                    new float[]{mult * headQuaternion[0], mult * headQuaternion[1], mult * headQuaternion[2]},
                    new float[]{1, 1, 1, 1}, eyePerspective);
        }
        if(detachedCamera){
            debugUiProgram.use();

            GLMatrix.setIdentity(modelMatrixDebug);
            Matrix.translateM(modelMatrixDebug, 0,
                    origin[0] - headDirVector[0] * headDistanceFromOrigin * .8f,
                    origin[1] - headDirVector[1] * headDistanceFromOrigin * .8f,
                    origin[2] - headDirVector[2] * headDistanceFromOrigin * .8f);
            rotateMatrixByOrientation(modelMatrixDebug, headQuaternion, getCurrentRotationMultiplier());
            drawCube(modelMatrixDebug, eyePerspective);

            // draw cube axes
            final float AXIS_SIZE = 4;
            float[] origin = new float[8]; origin[7] = 1;
            float[] xaxis = new float[8]; xaxis[4] = AXIS_SIZE; xaxis[7] = 1;
            float[] yaxis = new float[8]; yaxis[5] = AXIS_SIZE; yaxis[7] = 1;
            float[] zaxis = new float[8]; zaxis[6] = AXIS_SIZE; zaxis[7] = 1;
            Matrix.multiplyMV(origin, 0, modelMatrixDebug, 0, origin, 4);
            Matrix.multiplyMV(xaxis, 0, modelMatrixDebug, 0, xaxis, 4);
            Matrix.multiplyMV(yaxis, 0, modelMatrixDebug, 0, yaxis, 4);
            Matrix.multiplyMV(zaxis, 0, modelMatrixDebug, 0, zaxis, 4);
            drawLine(origin, xaxis, new float[]{1, 0, 0, 1}, eyePerspective);
            drawLine(origin, yaxis, new float[]{0, 1, 0, 1}, eyePerspective);
            drawLine(origin, zaxis, new float[]{0, 0, 1, 1}, eyePerspective);
        }

        /*
        RENDER BONE MODEL
         */
        if(modelSetup) {

            if (displayTexture && displayColorOverlay) {
                modelProgram = morphModelProgramTextureColor;
            } else if (displayTexture) {
                modelProgram = morphModelProgramTexture;
            } else if (displayColorOverlay) {
                modelProgram = morphModelProgramColor;
            } else {
                modelProgram = morphModelProgram;
            }

            modelProgram.use();
            // light uniform
            GLES20.glUniform3fv(modelProgram.getHandle("uLightPosition"), 1, lightPositionEye, 0);

            if(displaySideBySideMean){
                float currentPositionProportion = boneModel[activeComponentIndex].getPositionProportion();
                setActiveModelToMeanFrame();

                GLES20.glUniform3f(modelProgram.getHandle("uColorTint"), 0.7f, 1f, 0.7f);

                GLMatrix.setIdentity(modelMatrix);
                Matrix.translateM(modelMatrix, 0, boneModel[activeComponentIndex].getMaxRangeX() / 2, 0f, 0f);

                drawBoneModel(GLES20.GL_TRIANGLES, modelMatrix, eyePerspective, activeComponentIndex);
                boneModel[activeComponentIndex].setPositionProportion(currentPositionProportion);

                GLMatrix.setIdentity(modelMatrix);
                Matrix.translateM(modelMatrix, 0, -boneModel[activeComponentIndex].getMaxRangeX() / 2, 0f, 0f);
            }

            if (displayPointCloudMean) {
                float currentPositionProportion = boneModel[activeComponentIndex].getPositionProportion();
                setActiveModelToMeanFrame();

                GLES20.glUniform3f(modelProgram.getHandle("uColorTint"), 0.3f, 0.3f, 1f);

                drawBoneModel(GLES20.GL_POINTS, modelMatrix, eyePerspective, activeComponentIndex);
                boneModel[activeComponentIndex].setPositionProportion(currentPositionProportion);
            }

            if(displayAllComponents){
                // TODO DEBUG
                float alpha = activeSceneIndex == 1 ? 1 : timeBar.getProgress() / 100f;
                float[] dir = new float[3];
                // enable alpha blending
                GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

                // TODO DEBUG BREAKS with displayColorOverlay
                for (int i = 0; i < activeBoneModelData.components.length; i++) {
                    GLES20.glUniform3f(modelProgram.getHandle("uColorTint"),
                            ColorMap.r[i], ColorMap.g[i], ColorMap.b[i]);

                    GLMatrix.setIdentity(modelMatrix);

                    if(null != activeBoneModelData.scenes){
                        System.arraycopy(sceneTransformations[activeSceneIndex].get(), 0, modelMatrix, 0, 16);
                    }

                    // TODO use better check, this is dumb
                    if(null == activeBoneModelData.attachment) {
                        if (activeComponentIndex != i) {
                            FloatArrayVector3.set(dir, boneModel[i].getCenter());
                            FloatArrayVector3.normalize(dir);
                            FloatArrayVector3.mul(dir, dir, alpha * boneModel[i].getMaxRange());
                            Matrix.translateM(modelMatrix, 0, dir[0], dir[1], dir[2]);

                            GLES20.glEnable(GLES20.GL_BLEND);
                            GLES20.glUniform1f(modelProgram.getHandle("uAlpha"), 1-alpha);
                        } else {
                            GLES20.glDisable(GLES20.GL_BLEND);
                        }
                    } else {
                        // TODO DEBUG
//                        System.arraycopy(boneModelTransformations[i].get(), 0, modelMatrix, 0, 16);
                        GLMatrix.multiplyMM(modelMatrix, boneModelTransformations[i].get(), modelMatrix);
                    }

                    drawBoneModel(GLES20.GL_TRIANGLES, modelMatrix, eyePerspective, i);
                }
                GLMatrix.setIdentity(modelMatrix);

                debugUiProgram.use();
                if(null != muscleLines){
                    for(KeyframeLine muscleLine : muscleLines) {
                        drawLine(muscleLine.getPoint(0), muscleLine.getPoint(1),
                                new float[]{Math.min(1f, 1.5f * muscleLine.getActivation()), 0f, 0f, 1f}, eyePerspective);
                    }
                }

            } else {
                // TODO fix lighting on reflection
                if(null != activeBoneModelData.scenes){
                    System.arraycopy(sceneTransformations[activeSceneIndex].get(), 0, modelMatrix, 0, 16);
                }
                GLES20.glUniform3f(modelProgram.getHandle("uColorTint"), 1f, 1f, 1f);
                drawBoneModel(GLES20.GL_TRIANGLES, modelMatrix, eyePerspective, activeComponentIndex);
            }
        }

        /*
        RENDER MARKERS
         */
        if(markers.size() > 0){
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
            // enable alpha blending
            GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
            GLES20.glEnable(GLES20.GL_BLEND);

            debugUiProgram.use();

            synchronized (markers){
                for(int i = 0; i < markers.size(); i++){
                    GLMatrix.setIdentity(modelMatrixMarker);
                    setMarkerViewPlaneOrientedMatrix(modelMatrixMarker, markers.get(i));
                    drawMarker(modelMatrixMarker, eyePerspective);
                }
            }

            setBlendingToDefault();
            setDepthTestToDefault();
        }
    }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    ROTATION UTILS
     */

    private float getCurrentRotationMultiplier(){
        return doubleRotationSpeed ? 2 : 1;
    }

    private void setMatrixToOrientation(float[] matrix, float[] quaternion, float multiplier){
        Matrix.setRotateM(matrix, 0,
                multiplier * (float) (2.0 * Math.acos(quaternion[3]) * RAD_TO_DEG),
                quaternion[0], quaternion[1], quaternion[2]);
    }

    // tried decomposition - no effect, duh
    // tried multiple rotations - no effect, duh
    private void rotateMatrixByOrientation(float[] matrix, float[] quaternion, float multiplier){
        Matrix.rotateM(matrix, 0,
                multiplier * (float) (2.0 * Math.acos(quaternion[3]) * RAD_TO_DEG),
                quaternion[0], quaternion[1], quaternion[2]);
    }

    float[] cameraPos = new float[3];
    float[] billboardLook = new float[3];
    private void setMarkerViewPlaneOrientedMatrix(float[] matrix, float[] billboardPos){
        getCenterView(cameraPos);
        sub(billboardLook, cameraPos, billboardPos);

        /*Log.d(LOG_TAG, "cam " + Arrays.toString(cameraPos) + " bill " + Arrays.toString(billboardPos) +
                " look " + Arrays.toString(billboardLook) + " up " + Arrays.toString(headUpVector));*/

        // TODO investigate -z controversy
        /*Matrix.setLookAtM(matrix, 0,
                0, 0, 0,
                billboardLook[0], billboardLook[1], -billboardLook[2],
                headUpVector[0], headUpVector[1], headUpVector[2]);*/
        Matrix.translateM(matrix, 0, billboardPos[0], billboardPos[1], billboardPos[2]);
        rotateMatrixByOrientation(matrix, headQuaternion, getCurrentRotationMultiplier());
    }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    TOUCH INPUT
     */

    public void onCardboardTrigger(){
        // TODO synchronize with onDrawEye which modifies parameters like modelMatrix
        switch (controlType){
            case MARKERS:
                addMarker();
                break;
            case NAVIGATION:
                setRotationOrigin();
                break;
            case ANIMATION:
                toggleAutoAnimation();
                break;
        }
    }

    public void onSingleTap(){
        onCardboardTrigger();
    }

    public void onDoubleTap(){
        switch (controlType){
            case MARKERS:
                resetAllMarkers();
                break;
            case NAVIGATION:
                resetRotationOrigin();
                break;
            case ANIMATION:
                setAutoAnimation(false);
                activeSceneIndex = 0;
                setAutoAnimationStartTime(0);
                timeBar.setProgress(0);
                break;
        }
    }

    public void onScale(float scaleFactor){
        headDistanceFromOrigin = clamp(headDistanceFromOrigin / scaleFactor, minRange, MAX_HEAD_DISTANCE_FROM_ORIGIN);
    }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    REMOTE INPUT
     */
    private boolean remoteRotationPresent = false;
    private float[] remoteRotationQuaternion = new float[4];
    public void onRemoteOrientation(float x, float y, float z, float w){
        remoteRotationQuaternion[0] = x;
        remoteRotationQuaternion[1] = y;
        remoteRotationQuaternion[2] = z;
        remoteRotationQuaternion[3] = w;
        remoteRotationPresent = true;
    }

    private void getCenterView(float[] out){
        out[0] = -origin[0] - headDirVector[0] * headDistanceFromOrigin;
        out[1] = -origin[1] - headDirVector[1] * headDistanceFromOrigin;
        out[2] = -origin[2] - headDirVector[2] * headDistanceFromOrigin;
    }

    private void addMarker(){
        float[] rayStart = new float[]{0, 0, 0, 1};
        float[] collision = new float[]{0, 0, 0, 1};
        boolean rayCollided = rayPickModel(rayStart, collision);
        if(rayCollided){
            markers.add(collision);
            vibrator.vibrate(20);
        }
    }

    private void resetAllMarkers(){
        synchronized (markers) {
            markers.clear();
        }
    }

    public void setRotationOrigin(){
        float[] rayStart = new float[]{0, 0, 0, 1};
        float[] collision = new float[]{0, 0, 0, 1};
        boolean rayCollided = rayPickModel(rayStart, collision);
        if(rayCollided){
            float[] viewToCollision = new float[]{0, 0, 0, 0};
            sub(viewToCollision, collision, rayStart);
            headDistanceFromOrigin = FloatArrayVector3.length(viewToCollision);

            for(int i = 0; i < 3; i++){
                origin[i] = -collision[i];
            }

            // provide tactile feedback on successful origin change
            vibrator.vibrate(50);
        }
    }

    public void resetRotationOrigin(){
        origin[0] = 0;
        origin[1] = 0;
        origin[2] = 0;

        headDistanceFromOrigin = clamp(maxRange, minRange, MAX_HEAD_DISTANCE_FROM_ORIGIN);

        // provide tactile feedback on successful origin change
        vibrator.vibrate(50);
    }

    private boolean rayPickModel(float[] rayStart, float[] collision){
        getCenterView(rayStart);
        // rayEnd = rayStart + headDirVector
        float[] rayEnd = new float[]{0, 0, 0, 1};
        add(rayEnd, rayStart, headDirVector);

        if(modelSetup){
            return boneModel[activeComponentIndex].rayPick(
                    activeBoneModelData.components[activeComponentIndex].meanFrameIndex,
                    rayStart, rayEnd, modelMatrix, collision);
        } else {
            return cube.rayPick(0, rayStart, rayEnd, modelMatrixCubeRef, collision);
        }
    }

    public void toggleAutoAnimation(){
        autoAnimationSwitch.toggle();
    }

    public void setAutoAnimation(boolean enabled){
        // XOR
        if(enabled ^ autoAnimating) autoAnimationSwitch.toggle();
    }

    private float updateAndGetAutoAnimationFraction(){
        long totalTimeOneDirection = MS_PER_ALL_KEYFRAMES;
        long temp = getAutoAnimationTime() % (2 * totalTimeOneDirection);

        if(temp < totalTimeOneDirection){
            autoAnimatingDirectionForwards = true;
            return (float)temp / totalTimeOneDirection;
        } else {
            autoAnimatingDirectionForwards = false;
            return (float)(2*totalTimeOneDirection - temp) / totalTimeOneDirection;
        }
    }

    private long getAutoAnimationTime(){
        return System.currentTimeMillis() - autoAnimationStartTime;
    }

    private void setAutoAnimationStartTime(float fraction){
        if(autoAnimatingDirectionForwards){
            autoAnimationStartTime = System.currentTimeMillis() - (long)(fraction * MS_PER_ALL_KEYFRAMES);
        } else {
            autoAnimationStartTime = System.currentTimeMillis() - (long)((1 + (1-fraction)) * MS_PER_ALL_KEYFRAMES);
        }
    }

    public void advanceAnimation(float step) {
        if (!modelSetup) return;

        boneModel[activeComponentIndex].advancePosition(step);
        // TODO advance boneModelTransformations, muscleLines
        timeBar.setProgress((int) (boneModel[activeComponentIndex].getPositionProportion() * 100));
    }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    MODEL DRAWING
     */
    public void drawFloor(float[] perspective) {
        // mvp uniform
        GLES20.glUniformMatrix4fv(mHandleFloor, 1, false, modelMatrixFloor, 0);
        GLMatrix.multiplyMM(modelViewMatrix, viewMatrix, modelMatrixFloor);
        GLES20.glUniformMatrix4fv(mvHandleFloor, 1, false, modelViewMatrix, 0);
        GLMatrix.multiplyMM(modelViewProjectionMatrix, perspective, modelViewMatrix);
        GLES20.glUniformMatrix4fv(mvpHandleFloor, 1, false, modelViewProjectionMatrix, 0);

        // radius uniform
        GLES20.glUniform1f(radiusHandleFloor, maxRange / 2);

        floor.draw(positionHandleFloor, colorHandleFloor);
    }

    private void drawCube(float[] modelMatrixCube, float[] perspective){
        GLMatrix.multiplyMM(modelViewMatrix, viewMatrix, modelMatrixCube);
        GLES20.glUniformMatrix4fv(mvHandle, 1, false, modelViewMatrix, 0);
        GLMatrix.multiplyMM(modelViewProjectionMatrix, perspective, modelViewMatrix);
        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, modelViewProjectionMatrix, 0);

        cube.draw(positionHandle, colorHandle, textureCoordHandle, textureHandle);
    }

    private void drawMarker(float[] modelMatrixMarker, float[] perspective){
        GLMatrix.multiplyMM(modelViewMatrix, viewMatrix, modelMatrixMarker);
        GLES20.glUniformMatrix4fv(mvHandle, 1, false, modelViewMatrix, 0);
        GLMatrix.multiplyMM(modelViewProjectionMatrix, perspective, modelViewMatrix);
        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, modelViewProjectionMatrix, 0);

        marker.draw(positionHandle, colorHandle, textureCoordHandle, textureHandle);
    }

    private void drawLine(float[] p1, float[] p2, float[] color, float[] perspective){
        GLES20.glLineWidth(10);

        float[] modelMatrixLine = new float[16];
        GLMatrix.setIdentity(modelMatrixLine);
        // mvp
        Matrix.multiplyMM(modelViewMatrix, 0, viewMatrix, 0, modelMatrixLine, 0);
        GLES20.glUniformMatrix4fv(mvHandle, 1, false, modelViewMatrix, 0);
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, perspective, 0, modelViewMatrix, 0);
        GLES20.glUniformMatrix4fv(mvpHandle, 1, false, modelViewProjectionMatrix, 0);

        float[] lineVertices = new float[6];
        System.arraycopy(p1, 0, lineVertices, 0, 3);
        System.arraycopy(p2, 0, lineVertices, 3, 3);

        float[] lineColors = new float[8];
        System.arraycopy(color, 0, lineColors, 0, 4);
        System.arraycopy(color, 0, lineColors, 4, 4);

        GLES20.glEnableVertexAttribArray(positionHandle);
        GLES20.glEnableVertexAttribArray(colorHandle);

        // position
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
                3 * BYTES_PER_FLOAT, getAsFloatBuffer(lineVertices));

        // color
        GLES20.glVertexAttribPointer(colorHandle, 4, GLES20.GL_FLOAT, false,
                4 * BYTES_PER_FLOAT, getAsFloatBuffer(lineColors));

        // OK NOW DRAW!
        GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2);
    }


    private void drawBoneModel(int mode, float[] modelMatrixModel, float[] perspective, int componentIndex){
        // mvp uniform
        GLMatrix.multiplyMM(modelViewMatrix, viewMatrix, modelMatrixModel);
        GLES20.glUniformMatrix4fv(modelProgram.getHandle("uMVMatrix"), 1, false, modelViewMatrix, 0);
        GLMatrix.multiplyMM(modelViewProjectionMatrix, perspective, modelViewMatrix);
        GLES20.glUniformMatrix4fv(modelProgram.getHandle("uMVPMatrix"), 1, false, modelViewProjectionMatrix, 0);

        boneModel[componentIndex].draw(mode,
                modelProgram.getHandle("uTime"),
                modelProgram.getHandle("aPosition1"), modelProgram.getHandle("aPosition2"),
                modelProgram.getHandle("aNormal1"), modelProgram.getHandle("aNormal2"),
                modelProgram.getHandle("aColor"),
                modelProgram.getHandle("uMaxPositionDifferenceSq"),
                modelProgram.getHandle("uTexture"));
    }


    @Override
    public void onFinishFrame(Viewport viewport) { }

    @Override
    public void onRendererShutdown() { }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    UTILS
     */
    private static float clamp(float value, float min, float max){
        return Math.max(min, Math.min(value, max));
    }

    private static FloatBuffer getAsFloatBuffer(float[] data){
        ByteBuffer bb = ByteBuffer.allocateDirect(data.length * BYTES_PER_FLOAT);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(data);
        fb.position(0);
        return fb;
    }

    private static ShortBuffer getAsShortBuffer(short[] data){
        ByteBuffer bb = ByteBuffer.allocateDirect(data.length * BYTES_PER_SHORT);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer sb = bb.asShortBuffer();
        sb.put(data);
        sb.position(0);
        return sb;
    }

    /**
     * Checks if we've had an error inside of OpenGL ES, and if so what that error is.
     *
     * @param label Label to report in case of error.
     */
    private static void checkGLError(String label) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(LOG_TAG, label + ": glError " + error);
//            throw new RuntimeException(label + ": glError " + error);
        }
    }

    private static void checkGLExtensions(){
        Log.d(LOG_TAG, "Extensions: " + GLES20.glGetString(GLES20.GL_EXTENSIONS));
    }

    private static final int[] GL_INFO = new int[2];
    private static void checkGLInteger(String label, int parameterName){
        GLES20.glGetIntegerv(parameterName, GL_INFO, 0);
        Log.d(LOG_TAG, label + ": " + GL_INFO[0] + " .. " + GL_INFO[1]);
    }

    /*
    -------------------------------------------------------------------------------------------------------------------------
    DEMO
     */
    // TODO DRY in GestureCardboardView
    private final ControlMessage.ControlReceiver internalCommandAdapter = new ControlMessage.ControlReceiver() {
        @Override
        public void onOrient(float x, float y, float z, float w) {
            onRemoteOrientation(x, y, z, w);
        }

        @Override
        public void onScale(float scaleFactor) {
            CBModelRenderer.this.onScale(scaleFactor);
        }

        @Override
        public void onSetOrigin() {
            setRotationOrigin();
        }

        @Override
        public void onResetOrigin(){
            resetRotationOrigin();
        }

        @Override
        public void onToggleAutoAnimation() {
            toggleAutoAnimation();
        }

        @Override
        public void onAdvanceAnimation(float step) {
            advanceAnimation(step);
        }

        @Override
        public void onNewPreferences(boolean doubleRotation, boolean texture, boolean colorOverlay,
                                     boolean meanPointCloud, boolean meanSideBySide, boolean debugCube,
                                     boolean detachCamera, boolean morph, boolean allComponents, String navControl) {

            doubleRotationSpeed = doubleRotation;
            displayTexture = texture;
            displayColorOverlay = colorOverlay;
            displayPointCloudMean = meanPointCloud;
            displaySideBySideMean = meanSideBySide;
            displayAllComponents = allComponents;
            displayDebugCube = debugCube;
            detachedCamera = detachCamera;
            enableAnimation = morph;

            updateUiAfterPreferenceUpdate(navControl);
        }

        @Override
        public void onChooseModel(int index) {
            if(index >= 0 && index < ModelData.values().length){
                loadModelAsync(ModelData.values()[index]);
            }
        }
    };
}
