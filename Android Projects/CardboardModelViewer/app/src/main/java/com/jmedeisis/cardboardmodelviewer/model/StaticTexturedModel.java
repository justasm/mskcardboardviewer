package com.jmedeisis.cardboardmodelviewer.model;

import android.opengl.GLES20;

import com.jmedeisis.aglut.Texture;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * A static model with texture support.
 * <p>
 * Use {@link #draw(int, int, int, int, int)} if textured.
 * <p>
 * Created by Justas on 19/01/2015.
 */
public class StaticTexturedModel extends StaticModel {
    private Texture texture;
    private int textureCoordBufferHandle = -1;

    public StaticTexturedModel(ShortBuffer indices, FloatBuffer colors, FloatBuffer textureCoords, Texture texture, FloatBuffer... vertices){
        this(indices, colors, textureCoords, texture, vertices, null, true);
    }

    /** @see com.jmedeisis.cardboardmodelviewer.model.StaticModel#StaticModel(java.nio.ShortBuffer, java.nio.FloatBuffer, java.nio.FloatBuffer[], java.nio.FloatBuffer[], boolean) */
    public StaticTexturedModel(ShortBuffer indices, FloatBuffer colors, FloatBuffer textureCoords, Texture texture, FloatBuffer[] vertices, FloatBuffer[] normals, boolean compensateOffset){
        super(indices, colors, vertices, normals, compensateOffset);
        this.texture = texture;

        if(null != textureCoords){
            final int[] textureCoordBuffer = new int[1];
            GLES20.glGenBuffers(1, textureCoordBuffer, 0);

            textureCoords.position(0);
            textureCoordBufferHandle = textureCoordBuffer[0];
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, textureCoordBufferHandle);
            GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, textureCoords.capacity() * BYTES_PER_FLOAT,
                    textureCoords, GLES20.GL_STATIC_DRAW);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        }
    }

    public void draw(int shaderPositionHandle, int shaderColorHandle, int shaderTextureCoordHandle, int shaderTextureUniformHandle){
        bindPositions(0, shaderPositionHandle);
        bindColors(shaderColorHandle);
        if(textureCoordBufferHandle > -1) bindTextureCoords(shaderTextureCoordHandle);
        bindTexture(shaderTextureUniformHandle);
        bindIndicesDraw(GLES20.GL_TRIANGLES);
    }

    public void draw(int shaderPositionHandle, int shaderNormalHandle, int shaderColorHandle, int shaderTextureCoordHandle, int shaderTextureUniformHandle){
        bindPositions(0, shaderPositionHandle);
        bindNormals(0, shaderNormalHandle);
        bindColors(shaderColorHandle);
        if(textureCoordBufferHandle > -1) bindTextureCoords(shaderTextureCoordHandle);
        bindTexture(shaderTextureUniformHandle);
        bindIndicesDraw(GLES20.GL_TRIANGLES);
    }

    protected void bindTextureCoords(int shaderTextureCoordHandle){
        GLES20.glEnableVertexAttribArray(shaderTextureCoordHandle);
        // texture coords
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, textureCoordBufferHandle);
        GLES20.glVertexAttribPointer(shaderTextureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, 0);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    protected void bindTexture(int shaderTextureUniformHandle){
        final int textureUnit = 0;
        texture.bind(textureUnit);
        GLES20.glUniform1i(shaderTextureUniformHandle, textureUnit);
    }
}
