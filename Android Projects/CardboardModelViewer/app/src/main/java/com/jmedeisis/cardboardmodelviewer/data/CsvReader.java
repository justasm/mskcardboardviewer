package com.jmedeisis.cardboardmodelviewer.data;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Reads in rotation matrices exported from .mat as CSVs.
 * Super specific to exported files, not for general use.
 * Created by Justas on 22/04/2015.
 */
public class CsvReader {

    private static final String COMMA_SEPARATOR = ",";
    private static final String LOG_TAG = CsvReader.class.getSimpleName();

    public static List<float[]> readRotationMatricesFromCsv(Context context, String assetPath) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(assetPath)));
        String line;
        List<float[]> rotationMatrices = new ArrayList<>();
        float[] rotationMatrix = new float[16];
        int row = 0;
        while((line = reader.readLine()) != null){
            String[] lineSplit = line.split(COMMA_SEPARATOR);

            if(4 != lineSplit.length) throw new IllegalArgumentException("Invalid rotation matrix file, row has " +
                    lineSplit.length + " elements where 4 expected.");
//            Log.d(LOG_TAG, Arrays.deepToString(lineSplit));

            for(int i = 0; i < lineSplit.length; i++){
                // read row-major file into col-major matrix as expected by android.opengl.Matrix
                rotationMatrix[row + 4*i] = Float.parseFloat(lineSplit[i]);
            }
            ++row;

            if(row >= 4){
                rotationMatrices.add(Arrays.copyOf(rotationMatrix, rotationMatrix.length));
                row = 0;
            }
        }
        reader.close();
        return rotationMatrices;
    }

    /** assetPathFormat must be fileNameWithFormatCounter_%d_asSuch.txt, for example. */
    public static List<List<float[]>> readMuscleLinesFromCsvs(Context context, String assetPathFormat, int count) throws IOException {
        String line;
        List<List<float[]>> muscleLinesPerMuscle = new ArrayList<>(count);
        for(int i = 0; i < count; i++){
            String assetPath = String.format(Locale.UK, assetPathFormat, 1+i);
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(assetPath)));
            List<float[]> lineEndPoints = new ArrayList<>();
            float[] muscleLine = new float[6];
            while((line = reader.readLine()) != null){
                String[] lineSplit = line.split(COMMA_SEPARATOR);

                if(6 != lineSplit.length) throw new IllegalArgumentException("Two endpoints expected per line, row has " +
                        lineSplit.length + " elements where 6 expected.");

                for(int j = 0; j < lineSplit.length; j++){
                    muscleLine[j] = Float.parseFloat(lineSplit[j]);
                }

                lineEndPoints.add(Arrays.copyOfRange(muscleLine, 0, 3));
                lineEndPoints.add(Arrays.copyOfRange(muscleLine, 3, 6));
            }
            reader.close();
            muscleLinesPerMuscle.add(lineEndPoints);
        }
        return muscleLinesPerMuscle;
    }

    public static List<float[]> readMuscleActivationsFromCsv(Context context, String assetPath, int frames) throws IOException {
        String line;
        List<float[]> muscleActivations = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(assetPath)));
        float[] activationFrames = new float[frames];
        while((line = reader.readLine()) != null){
            String[] lineSplit = line.split(COMMA_SEPARATOR);

            if(frames != lineSplit.length) throw new IllegalArgumentException("Insufficient activation coefficient frames present," +
                    lineSplit.length + " elements where " + frames + " expected.");

            for(int i = 0; i < lineSplit.length; i++){
                activationFrames[i] = Float.parseFloat(lineSplit[i]);
            }
            muscleActivations.add(Arrays.copyOf(activationFrames, activationFrames.length));
        }
        reader.close();
        return muscleActivations;
    }
}
