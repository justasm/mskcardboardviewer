package com.jmedeisis.cardboardmodelviewer.data;

/**
 * Based on lines colormap from MatLab.
 *
 * Created by Justas on 24/02/2015.
 */
public class ColorMap {
    public static final float[] r = new float[]{
            0f,    0.8500f,    0.9290f,    0.4940f,    0.4660f,    0.3010f,    0.6350f,         0f,    0.8500f,    0.9290f,    0.4940f,    0.4660f
    };
    public static final float[] g = new float[]{
            0.4470f,    0.3250f,    0.6940f,    0.1840f,    0.6740f,    0.7450f,    0.0780f,    0.4470f,    0.3250f,    0.6940f,    0.1840f,    0.6740f
    };
    public static final float[] b = new float[]{
            0.7410f,    0.0980f,    0.1250f,    0.5560f,    0.1880f,    0.9330f,    0.1840f,    0.7410f,    0.0980f,    0.1250f,    0.5560f,    0.1880f
    };
}
