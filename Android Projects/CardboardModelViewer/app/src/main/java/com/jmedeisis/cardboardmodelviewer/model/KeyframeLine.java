package com.jmedeisis.cardboardmodelviewer.model;

import java.util.List;

/**
 * Represents a keyframe-animated line. Call {@link #getPoint(int)} to retrieve for rendering.
 *
 * Created by Justas on 24/04/2015.
 */
public class KeyframeLine {

    private List<float[]> lineEndPoints;
    private float[] normalisedActivationValues;
    private final int maxFrame;
    private float frame = 0;

    public KeyframeLine(List<float[]> lineEndPoints, float[] normalisedActivationValues){
        this.lineEndPoints = lineEndPoints;
        this.normalisedActivationValues = normalisedActivationValues;
        maxFrame = (lineEndPoints.size() / 2) - 1;
    }

    public void setPositionProportion(float proportion){
        setFrame(proportion * maxFrame);
    }

    public void advancePosition(float step){
        setFrame(Math.max(0, Math.min(frame + step, maxFrame)));
    }

    private void setFrame(float frame){
        this.frame = frame;
    }

    /** index = 0 | 1 */
    public float[] getPoint(int index){
        return lineEndPoints.get((int)(2 * frame + index));
    }

    public float getActivation(){
        return normalisedActivationValues[(int) frame];
    }
}
