package com.jmedeisis.cardboardmodelviewer.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Justas on 26/05/2015.
 */
public class BufferUtils {

    private static final int BYTES_PER_SHORT = 2;
    private static final int BYTES_PER_FLOAT = 4;

    public static FloatBuffer getAsFloatBuffer(float[] data){
        ByteBuffer bb = ByteBuffer.allocateDirect(data.length * BYTES_PER_FLOAT);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(data);
        fb.position(0);
        return fb;
    }

    public static ShortBuffer getAsShortBuffer(short[] data){
        ByteBuffer bb = ByteBuffer.allocateDirect(data.length * BYTES_PER_SHORT);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer sb = bb.asShortBuffer();
        sb.put(data);
        sb.position(0);
        return sb;
    }
}
