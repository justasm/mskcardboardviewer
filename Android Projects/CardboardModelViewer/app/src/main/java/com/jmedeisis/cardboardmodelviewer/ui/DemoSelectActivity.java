package com.jmedeisis.cardboardmodelviewer.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.jmedeisis.cardboardmodelviewer.R;
import com.jmedeisis.common.state.DemoState;

public class DemoSelectActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_select);

        findViewById(R.id.demo_scapula_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.createIntent(DemoSelectActivity.this, DemoState.Type.SCAPULA));
            }
        });
        findViewById(R.id.demo_knee_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.createIntent(DemoSelectActivity.this, DemoState.Type.MENISCUS));
            }
        });
        findViewById(R.id.demo_shoulder_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.createIntent(DemoSelectActivity.this, DemoState.Type.SHOULDER));
            }
        });
        findViewById(R.id.demo_custom_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(MainActivity.createIntent(DemoSelectActivity.this, DemoState.Type.DEBUG));
            }
        });
    }

}
