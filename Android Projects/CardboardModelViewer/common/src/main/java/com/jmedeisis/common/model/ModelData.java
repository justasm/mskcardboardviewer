package com.jmedeisis.common.model;

/**
 * Represents the available models for loading and display.
 *
 * Created by Justas on 30/11/2014.
 */
public enum ModelData {

    SCAPULA_SC_N2_P2(
        "Scapula -2<σ<2",
        new Scene[]{
            new Scene(new Transformation("ssm/scapula_scaled/transform/t_scapula.txt"), 0)
        },
        new Component(
            "1st",
            "1st",
            "ssm/scapula_scaled/scapulas_out_mode_1_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        ),
        new Component(
            "2nd",
            "2nd",
            "ssm/scapula_scaled/scapulas_out_mode_2_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        ),
        new Component(
            "3rd principal components",
            "3rd",
            "ssm/scapula_scaled/scapulas_out_mode_3_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        )
    ),
    FEMUR_N2_P2(
        "Femur -2<σ<2",
        new Component(
            "1st",
            "1st",
            "ssm/femur/femur_out_mode_1_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        ),
        new Component(
            "2nd principal components",
            "2nd",
            "ssm/femur/femur_out_mode_2_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        )
    ),
    TIBIA_N2_P2(
        "Tibia & Fibula -2<σ<2",
        new Component(
            "1st",
            "1st",
            "ssm/tibia_fibula/tibia_out_mode_1_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        ),
        new Component(
            "2nd principal components",
            "2nd",
            "ssm/tibia_fibula/tibia_out_mode_2_",
            ModelDataUtil.STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP,
            4
        )
    ),
    FEMORAL_HEAD(
        "Femoral head",
        new Component(
            "",
            "",
            "ssm/cam_hip/ch_",
            new String[]{
                "mn.vtk",
                "newf100pn.vtk"
            },
            0
        )
    ),
    KNEE_ATLAS(
        "Knee Atlas",
        new Scene[]{
                new Scene(new Transformation("atlas/knee/transform/r_knee.txt"), 0),
                new Scene(new Transformation("atlas/knee/transform/r_knee.txt"), 0)
        },
        new Component(
            "Medial Meniscus",
            "Med Men",
            "atlas/knee/medial_meniscus_tear_",
            new String[]{
                "closed.vtk",
                "open.vtk"
            },
            0
        ),
        new Component(
            "Lateral Meniscus",
            "Lat Men",
            "atlas/knee/Model_81_lateral_meniscus_decimated07_bin.vtk"
        ),
        new Component(
            "Femur",
            "Femur",
            "atlas/knee/Model_95_femur_decimated_bin.vtk"
        ),
        new Component(
            "Patella",
            "Patella",
            "atlas/knee/Model_98_patella_decimated08_bin.vtk"
        ),
        new Component(
            "Tibia",
            "Tibia",
            "atlas/knee/Model_96_tibia_decimated_bin.vtk"
        ),
        new Component(
            "Fibula",
            "Fibula",
            "atlas/knee/Model_97_fibula_decimated07_bin.vtk"
        )
    ),
    KNEE_ATLAS2(
        "Knee Atlas & Body",
        new Scene[]{
            new Scene(new Transformation("atlas/knee/transform/r_knee.txt"), 0),
            new Scene(new Transformation("atlas/knee/transform/r_knee.txt"), 0)
        },
        new Component(
            "Medial Meniscus",
            "Med Men",
            "atlas/knee/medial_meniscus_tear_",
            new String[]{
                "closed.vtk",
                "open.vtk"
            },
            0
        ),
        new Component(
            "Body",
            "Body",
            "atlas/knee/body.vtk"
        ),
        new Component(
            "Lateral Meniscus",
            "Lat Men",
            "atlas/knee/Model_81_lateral_meniscus_decimated07_bin.vtk"
        ),
        new Component(
            "Femur",
            "Femur",
            "atlas/knee/Model_95_femur_decimated_bin.vtk"
        ),
        new Component(
            "Patella",
            "Patella",
            "atlas/knee/Model_98_patella_decimated08_bin.vtk"
        ),
        new Component(
            "Tibia",
            "Tibia",
            "atlas/knee/Model_96_tibia_decimated_bin.vtk"
        ),
        new Component(
            "Fibula",
            "Fibula",
            "atlas/knee/Model_97_fibula_decimated07_bin.vtk"
        )
    ),
    SHOULDER_DYNAMICS(
        "Shoulder Dynamics",
        new AnimatedAttachment("atlas/shoulder/muscle/line_data1_%d.txt", "atlas/shoulder/muscle/activation.txt"),
        new Component(
            "Spine",
            "Spine",
            "atlas/shoulder/spine.vtk"
        ),
        new Component(
            "Ribs",
            "Ribs",
            "atlas/shoulder/ribs.vtk"
        ),
        new Component(
            "Scapula",
            "Scapula",
            "atlas/shoulder/scapula.vtk",
            new Transformation("atlas/shoulder/transform/r_scapula.txt")
        ),
        new Component(
            "Humerus",
            "Humerus",
            "atlas/shoulder/humerus.vtk",
            new Transformation("atlas/shoulder/transform/r_humerus.txt")
        ),
        new Component(
            "Radius",
            "Radius",
            "atlas/shoulder/radius.vtk",
            new Transformation("atlas/shoulder/transform/r_radius.txt")
        ),
        new Component(
            "Ulna",
            "Ulna",
            "atlas/shoulder/ulna.vtk",
            new Transformation("atlas/shoulder/transform/r_ulna.txt")
        ),
        new Component(
            "Clavicle",
            "Clavicle",
            "atlas/shoulder/clavicle.vtk",
            new Transformation("atlas/shoulder/transform/r_clavicle.txt")
        )
    );

    public final String label;
    public final Component[] components;
    private ModelData(String label, Component component, Component... otherComponents){
        this.label = label;
        this.components = new Component[1 + otherComponents.length];
        this.components[0] = component;
        System.arraycopy(otherComponents, 0, this.components, 1, otherComponents.length);
    }

    public Scene[] scenes;
    private ModelData(String label, Scene[] scenes, Component component, Component... otherComponents){
        this(label, component, otherComponents);
        this.scenes = scenes;
    }

    public AnimatedAttachment attachment;
    private ModelData(String label, AnimatedAttachment attachment, Component component, Component... otherComponents){
        this(label, component, otherComponents);
        this.attachment = attachment;
    }

    public static enum SceneType {
        ZOOM,
        EXPLODE
    }

    public static class Scene {
        public final int[] componentIndices;
        public final Transformation transformation;

        public Scene(){
            this.componentIndices = new int[0];
            transformation = null;
        }

        public Scene(Transformation transformation, int componentIndex, int... otherComponentIndices){
            this.transformation = transformation;
            this.componentIndices = new int[1 + otherComponentIndices.length];
            this.componentIndices[0] = componentIndex;
            System.arraycopy(otherComponentIndices, 0, this.componentIndices, 1, otherComponentIndices.length);
        }
    }

    public static class Component {
        public final String label;
        public final String shortLabel;
        private final String frameAssetPathPrefix;
        private final String[] frameAssetPathSuffixes;
        public final int meanFrameIndex;
        public final Transformation transformation;

        public Component(String label, String shortLabel, String frameAssetPath){
            this(label, shortLabel, "", new String[]{frameAssetPath}, 0);
        }

        public Component(String label, String shortLabel, String frameAssetPath, Transformation transformation){
            this(label, shortLabel, "", new String[]{frameAssetPath}, 0, transformation);
        }

        public Component(String label, String shortLabel, String frameAssetPathPrefix,
                            String[] frameAssetPathSuffixes, int meanFrameIndex){
            this(label, shortLabel, frameAssetPathPrefix, frameAssetPathSuffixes, meanFrameIndex, null);
        }

        public Component(String label, String shortLabel, String frameAssetPathPrefix,
                         String[] frameAssetPathSuffixes, int meanFrameIndex, Transformation transformation){
            this.label = label;
            this.shortLabel = shortLabel;
            this.frameAssetPathPrefix = frameAssetPathPrefix;
            this.frameAssetPathSuffixes = frameAssetPathSuffixes;
            this.meanFrameIndex = meanFrameIndex;
            this.transformation = transformation;
        }

        public int getFrameCount(){
            return frameAssetPathSuffixes.length;
        }

        public String getAssetPath(int frame){
            return frameAssetPathPrefix + frameAssetPathSuffixes[frame];
        }
    }

    public static class Transformation {
        public final String rotationMatrixPath;

        public Transformation(String rotationMatrixPath){
            this.rotationMatrixPath = rotationMatrixPath;
        }
    }

    // TODO rename
    public static class AnimatedAttachment {
        public final String assetPathFormat;
        public final String activationAssetPath;

        public AnimatedAttachment(String assetPathFormat, String activationAssetPath){
            this.assetPathFormat = assetPathFormat;
            this.activationAssetPath = activationAssetPath;
        }
    }
}
