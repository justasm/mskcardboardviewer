package com.jmedeisis.common.bt;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Portions of this code are modifications based on work created and shared by the
 * Android Open Source Project and used according to terms described in the
 * <a href="https://creativecommons.org/licenses/by/2.5/">
 *     Creative Commons 2.5 Attribution License</a>.
 * <p>
 * Portions of code adapted from
 * http://developer.android.com/guide/topics/connectivity/bluetooth.html
 *
 * Created by Justas on 15/11/2014.
 */
public class BTConnection {

    private static final String LOG_TAG = BTConnection.class.getSimpleName();

    // Name for the SDP record when creating server socket
    private static final String NAME_SECURE = "ReceiverSecure";
    private static final String NAME_INSECURE = "ReceiverInsecure";

    // Unique UUID for this application
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("6ace4976-5216-49e9-b88a-d23c38d329c9");
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("d99c0b67-f7d4-4e5c-ad6a-a6192b9eba27");

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;

    private static final int ONGOING_NOTIFICATION_ID = 1;

    /**
     * Custom Bluetooth message packet protocol.<p>
     * Data format:
     * <li>DATA_HEADER		[1 byte, constant == 0xDA]
     * <li>DATA_LENGTH		[1 byte]
     * <li>DATA				[DATA_LENGTH bytes]
     */
    private static final byte DATA_HEADER = (byte) 0xDA;

    private BluetoothAdapter btAdapter;
    private ConnectThread connectThread;
    private ConnectedThread connectedThread;
    private AcceptThread acceptThread;

    private List<Messenger> messengers;

    public static enum State {
        NONE,
        LISTENING,
        CONNECTING,
        CONNECTED;
    }
    private State state;

    public BTConnection(){
        state = State.NONE;

        messengers = new ArrayList<Messenger>();

        btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public void addListener(Messenger messenger){
        messengers.add(messenger);
    }

    public void removeListener(Messenger messenger){
        messengers.remove(messenger);
    }

    /** Notifies any Context that supplied a Messenger with the Intent starting this service. */
    private void notifyListeners(Message msg){
        for(int i = 0; i < messengers.size(); i++){
            try {
                messengers.get(i).send(i == 0? msg : Message.obtain(msg)); // TODO figure out whether making copies is necessary?
            } catch (RemoteException e) {
                Log.e(LOG_TAG, "Failed to send message to " + messengers.get(i) + ", deleting.", e);
                messengers.remove(i);
                --i;
            }
        }
    }

    public void destroy(){
        // Clean up all threads
        if (connectThread != null) {connectThread.cancel(); connectThread = null;}
        if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}
        if (acceptThread != null) {acceptThread.cancel(); acceptThread = null;}
        setState(State.NONE);
    }

    /**
     * Set the current state of the connection.
     */
    private synchronized void setState(State newState) {
        Log.d(LOG_TAG, "setState() " + state + " -> " + newState);
        state = newState;

        // Notify any listening activity of the state change.
        notifyListeners(Message.obtain(null, MESSAGE_STATE_CHANGE, newState));
    }

    /** @return the current connection state. */
    public synchronized State getState() {
        return state;
    }

    public synchronized void listen(){
        // Cancel any thread attempting a connection
        if (connectThread != null) {connectThread.cancel(); connectThread = null;}

        // Cancel any thread currently running a connection
        if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

        setState(State.LISTENING);

        // Start the thread to listen on a BluetoothServerSocket
        if(acceptThread == null){
            acceptThread = new AcceptThread(true);
            acceptThread.start();
        }
    }

    public synchronized void connect(BluetoothDevice device, boolean secure) {
        // Cancel any thread attempting to make a connection
        if (State.CONNECTING == state) {
            if (connectThread != null) {connectThread.cancel(); connectThread = null;}
        }

        // Cancel any thread currently running a connection
        if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

        // Start the thread to connect with the given device
        connectThread = new ConnectThread(device, secure);
        connectThread.start();
        setState(State.CONNECTING);
    }

    /**
     * Starts the ConnectedThread to begin managing a Bluetooth connection.
     * @param socket  BluetoothSocket on which the connection was made
     * @param device  BluetoothDevice that has been connected
     */
    private synchronized void onConnectionSuccess(BluetoothSocket socket, BluetoothDevice device) {
        // Cancel the thread that completed the connection
        if (connectThread != null) {connectThread.cancel(); connectThread = null;}

        // Cancel any thread currently running a connection
        if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

        // Cancel accept thread as we're only interested in communicating with a single device
        if (acceptThread != null) {acceptThread.cancel(); acceptThread = null;}

        // Start the thread to manage the connection and perform transmissions
        connectedThread = new ConnectedThread(socket);
        connectedThread.start();

        setState(State.CONNECTED);
    }

    /**
     * Indicates that the connection attempt failed.
     */
    private void onConnectionFail() {
        // TODO handle better..

        setState(State.NONE);
    }

    /**
     * Indicates that the connection was lost.
     */
    private void onConnectionLost() {
        // TODO handle better..

        setState(State.NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void writeRaw(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (State.CONNECTED != state) return;
            r = connectedThread;
        }
//        Log.d(LOG_TAG, "Writing " + getHex(out));
        // Perform the write unsynchronized
        r.write(out);
    }

    // DATA_HEADER & message_length
    private static final int PACKAGE_PREFIX_LENGTH = 1 + 1;
    /**
     * TODO formalize & document
     * @param out
     */
    public void writePackaged(byte[] out){
        // Package up
        ByteArrayBuffer bos = new ByteArrayBuffer(PACKAGE_PREFIX_LENGTH + out.length);
        bos.append(DATA_HEADER);
        bos.append(out.length);
        bos.append(out, 0, out.length);

        writeRaw(bos.toByteArray());
    }
    private byte[] unpack(byte[] in, int fullLength){
        int messageLength = fullLength - PACKAGE_PREFIX_LENGTH;
        byte[] messageRead = new byte[messageLength];
        System.arraycopy(in, PACKAGE_PREFIX_LENGTH, messageRead, 0, messageLength);
        return messageRead;
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket socket;
        private final BluetoothDevice device;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            this.device = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(secure ? MY_UUID_SECURE : MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Failed to create RFCOMM socket.", e);
            }
            socket = tmp;
        }

        public void run() {
            Log.i(LOG_TAG, "BEGIN mConnectThread Socket");
            setName("ConnectThread");

            // Always cancel discovery because it will slow down a connection
            btAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                socket.connect();
            } catch (IOException e) {
                // Close the socket
                try {
                    socket.close();
                } catch (IOException e2) {
                    Log.e(LOG_TAG, "unable to close() socket during connection failure", e2);
                }
                onConnectionFail();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BTConnection.this) {
                connectThread = null;
            }

            // Start the connected thread
            onConnectionSuccess(socket, device);
        }

        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, "close() of socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with the remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket socket;
        private final InputStream inStream;
        private final OutputStream outStream;

        public ConnectedThread(BluetoothSocket socket) {
            this.socket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inStream = tmpIn;
            outStream = tmpOut;
        }

        public void run() {
            byte[] tempBuffer = new byte[1024];  // buffer store for the stream

            byte[] messageBuffer = new byte[512];
            boolean messageInProgress = false;
            int readMessagePosition = 0;
            int readMessageLimit = 512;

            Log.d("ConnectedThread", "Started running.");

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    int numBytesRead = inStream.read(tempBuffer); // blocks until we actually get something

//                    byte[] logArray = new byte[numBytesRead];
//                    System.arraycopy(tempBuffer, 0, logArray, 0, numBytesRead);
//					Log.d("ConnectedThread", "Read " + getHex(logArray));

                    for(int i = 0; i < numBytesRead; i++){
                        byte b = tempBuffer[i];

                        if(messageInProgress){
//							Log.d("ConnectedThread", "Message in progress, received byte " + b + " for position " + readMessagePosition);

                            // In process of reading message, put into buffer
                            messageBuffer[readMessagePosition++] = b;

                            if(readMessagePosition >= readMessageLimit){
                                // We're done with one message!
//								Log.d("ConnectedThread", "MESSAGE DONE");

                                // Send the obtained bytes to any listeners
                                notifyListeners(Message.obtain(null, MESSAGE_READ, unpack(messageBuffer, readMessagePosition)));

                                // Reset all values
                                readMessagePosition = 0;
                                readMessageLimit = 512;
                                messageInProgress = false;
                            } else if(readMessagePosition == 2){
                                // Second byte will indicate our message length
                                readMessageLimit = 1 + 1 + (b & 0xFF); // header byte + length byte + data

//								Log.d("ConnectedThread", "MESSAGE LIMIT " + readMessageLimit);
                            }
                        } else if(DATA_HEADER == b){
//							Log.d("ConnectedThread", "NEW MESSAGE byte " + b);
                            // We're starting a new message!
                            messageInProgress = true;

                            // Store header in message as well
                            messageBuffer[readMessagePosition++] = b;
                        } else {
                            Log.i("ConnectedThread", "Dropping byte " + b);
                            // Not part of our message or our header, thus drop it!
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    onConnectionLost();
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                outStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                socket.close();
            } catch (IOException e) { }
        }
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket serverSocket;
        private String socketType;

        public AcceptThread(boolean secure) {
            BluetoothServerSocket tmp = null;
            socketType = secure ? "Secure":"Insecure";

            // Create a new listening server socket
            try {
                if (secure) {
                    tmp = btAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE);
                } else {
                    tmp = btAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                Log.e(LOG_TAG, "Socket Type: " + socketType + "listen() failed", e);
            }
            serverSocket = tmp;
        }

        public void run() {
            Log.i(LOG_TAG, "Socket Type: " + socketType +
                    "BEGIN mAcceptThread" + this);
            setName("AcceptThread" + socketType);

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (state != BTConnection.State.CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = serverSocket.accept();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Socket Type: " + socketType + "accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BTConnection.this) {
                        switch (state) {
                            case LISTENING:
                            case CONNECTING:
                                // Situation normal. Start the connected thread.
                                onConnectionSuccess(socket, socket.getRemoteDevice()/*, socketType*/);
                                break;
                            case NONE:
                            case CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(LOG_TAG, "Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            Log.i(LOG_TAG, "END mAcceptThread, socket Type: " + socketType);
        }

        public void cancel() {
            Log.i(LOG_TAG, "Socket Type" + socketType + "cancel " + this);
            try {
                serverSocket.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Socket Type" + socketType + "close() of server failed", e);
            }
        }
    }


    /** Converts a byte array to a hex string representation. */
    private static String getHex(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (byte b : bytes) {
            sb.append("0x" + String.format("%02X, ", b));
        }
        sb.delete(sb.length()-2, sb.length());
        sb.append("]");
        return sb.toString();
    }
}