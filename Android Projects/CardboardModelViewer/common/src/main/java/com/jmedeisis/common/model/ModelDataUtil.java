package com.jmedeisis.common.model;

/**
 * Created by Justas on 03/02/2015.
 */
public class ModelDataUtil {
    static final String[] STD_N2_P2_9_POINT_SWEEP_HALF_STD_STEP = new String[]{
            "00_sigma_n2_0.vtk",
            "01_sigma_n1_5.vtk",
            "02_sigma_n1_0.vtk",
            "03_sigma_n0_5.vtk",
            "04_sigma_p0_0.vtk",
            "05_sigma_p0_5.vtk",
            "06_sigma_p1_0.vtk",
            "07_sigma_p1_5.vtk",
            "08_sigma_p2_0.vtk",
    };

    private ModelDataUtil(){ }
}
