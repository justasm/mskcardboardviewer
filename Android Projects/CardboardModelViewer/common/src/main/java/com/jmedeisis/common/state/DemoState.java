package com.jmedeisis.common.state;

import android.content.Context;

import com.jmedeisis.btcommon.R;
import com.jmedeisis.common.bt.ControlMessage;
import com.jmedeisis.common.model.ModelData;

/**
 * Created by Justas on 01/05/2015.
 */
public class DemoState {
    public enum Type {
        DEBUG,
        SCAPULA,
        MENISCUS,
        SHOULDER
    }
    public final Type type;

    public DemoState(Type type){
        this.type = type;
    }

    public void apply(Context context, ControlMessage.ControlReceiver receiver){
        receiver.onResetOrigin();
        switch (type){
            case DEBUG:
                break;
            case SCAPULA:
                receiver.onNewPreferences(
                        false,  // double rot
                        false,  // texture
                        true,   // color overlay
                        false,  // mean point cloud
                        false,  // mean side by side
                        false,  // debug cube
                        false,  // detach camera
                        true,   // morph animation
                        false,  // display all components at once
                        context.getString(R.string.pref_magnet_tap_control_animation)
                );
                receiver.onChooseModel(ModelData.SCAPULA_SC_N2_P2.ordinal());
                break;
            case MENISCUS:
                receiver.onNewPreferences(
                        false,  // double rot
                        false,  // texture
                        false,  // color overlay
                        false,  // mean point cloud
                        false,  // mean side by side
                        false,  // debug cube
                        false,  // detach camera
                        true,   // morph animation
                        true,   // display all components at once
                        context.getString(R.string.pref_magnet_tap_control_animation)
                );
                receiver.onChooseModel(ModelData.KNEE_ATLAS.ordinal());
                break;
            case SHOULDER:
                receiver.onNewPreferences(
                        false,  // double rot
                        false,  // texture
                        false,  // color overlay
                        false,  // mean point cloud
                        false,  // mean side by side
                        false,  // mean debug cube
                        false,  // mean detach camera
                        true,   // morph animation
                        true,    // display all components at once
                        context.getString(R.string.pref_magnet_tap_control_animation)
                );
                receiver.onChooseModel(ModelData.SHOULDER_DYNAMICS.ordinal());
                break;
        }
    }
}
