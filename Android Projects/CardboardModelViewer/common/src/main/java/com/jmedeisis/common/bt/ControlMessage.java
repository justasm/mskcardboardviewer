package com.jmedeisis.common.bt;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

/**
 * Created by Justas on 07/12/2014.
 */
public class ControlMessage {
    public enum Type {
        ORIENT,
        SCALE,
        SET_ORIGIN,
        RESET_ORIGIN,
        TOGGLE_ANIMATION,
        ADVANCE_ANIMATION,
        PREFERENCES,
        CHOOSE_MODEL
    }

    public static byte[] encodeOrient(float x, float y, float z, float w){
        return ByteBuffer.allocate(4 * 5).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.ORIENT.ordinal())
                .putFloat(x).putFloat(y).putFloat(z).putFloat(w).array();
    }

    public static byte[] encodeScale(float scaleFactor){
        return ByteBuffer.allocate(4 * 2).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.SCALE.ordinal())
                .putFloat(scaleFactor).array();
    }

    public static byte[] encodeSetOrigin(){
        return ByteBuffer.allocate(4 * 1).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.SET_ORIGIN.ordinal()).array();
    }

    public static byte[] encodeResetOrigin(){
        return ByteBuffer.allocate(4 * 1).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.RESET_ORIGIN.ordinal()).array();
    }

    public static byte[] encodeToggleAutoAnimation(){
        return ByteBuffer.allocate(4 * 1).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.TOGGLE_ANIMATION.ordinal()).array();
    }

    public static byte[] encodeAdvanceAnimation(float step){
        return ByteBuffer.allocate(4 * 2).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.ADVANCE_ANIMATION.ordinal())
                .putFloat(step).array();
    }

    public static byte[] encodePreferences(
            boolean doubleRotation,
            boolean texture,
            boolean colorOverlay,
            boolean meanPointCloud,
            boolean meanSideBySide,
            boolean debugCube,
            boolean detachCamera,
            boolean morph,
            boolean allComponents,
            String navControl) {
        byte[] navControlBytes = navControl.getBytes(Charset.forName("UTF-8"));
        return ByteBuffer.allocate(4 + 9 + 4 + navControlBytes.length).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.PREFERENCES.ordinal())
                .put(doubleRotation ? (byte) 1 : 0)
                .put(texture ? (byte) 1 : 0)
                .put(colorOverlay ? (byte) 1 : 0)
                .put(meanPointCloud ? (byte) 1 : 0)
                .put(meanSideBySide ? (byte) 1 : 0)
                .put(debugCube ? (byte) 1 : 0)
                .put(detachCamera ? (byte) 1 : 0)
                .put(morph ? (byte) 1 : 0)
                .put(allComponents ? (byte) 1 : 0)
                .putInt(navControlBytes.length)
                .put(navControlBytes).array();
    }

    public static byte[] encodeChooseModel(int modelIndex){
        return ByteBuffer.allocate(4 * 2).order(ByteOrder.BIG_ENDIAN)
                .putInt(Type.CHOOSE_MODEL.ordinal())
                .putInt(modelIndex).array();
    }

    public static interface ControlReceiver {
        public void onOrient(float x, float y, float z, float w);
        public void onScale(float scaleFactor);
        public void onSetOrigin();
        public void onResetOrigin();
        public void onToggleAutoAnimation();
        public void onAdvanceAnimation(float step);
        public void onNewPreferences(
                boolean doubleRotation,
                boolean texture,
                boolean colorOverlay,
                boolean meanPointCloud,
                boolean meanSideBySide,
                boolean debugCube,
                boolean detachCamera,
                boolean morph,
                boolean allComponents,
                String navControl);
        public void onChooseModel(int index);
    }

    public static void decode(byte[] data, ControlReceiver receiver){
        ByteBuffer buffer = ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN);
        final Type type = Type.values()[buffer.getInt()];
        switch (type){
            case ORIENT:
                receiver.onOrient(buffer.getFloat(), buffer.getFloat(), buffer.getFloat(), buffer.getFloat());
                break;
            case SCALE:
                receiver.onScale(buffer.getFloat());
                break;
            case SET_ORIGIN:
                receiver.onSetOrigin();
                break;
            case RESET_ORIGIN:
                receiver.onResetOrigin();
                break;
            case TOGGLE_ANIMATION:
                receiver.onToggleAutoAnimation();
                break;
            case ADVANCE_ANIMATION:
                receiver.onAdvanceAnimation(buffer.getFloat());
                break;
            case PREFERENCES:
                boolean doubleRotation = buffer.get() > 0;
                boolean texture = buffer.get() > 0;
                boolean colorOverlay = buffer.get() > 0;
                boolean meanPointCloud = buffer.get() > 0;
                boolean meanSideBySide = buffer.get() > 0;
                boolean debugCube = buffer.get() > 0;
                boolean detachCamera = buffer.get() > 0;
                boolean morph = buffer.get() > 0;
                boolean allComponents = buffer.get() > 0;

                byte[] navControlBytes = new byte[buffer.getInt()];
                buffer.get(navControlBytes);

                receiver.onNewPreferences(
                        doubleRotation,
                        texture,
                        colorOverlay,
                        meanPointCloud,
                        meanSideBySide,
                        debugCube,
                        detachCamera,
                        morph,
                        allComponents,
                        new String(navControlBytes)
                );

                break;
            case CHOOSE_MODEL:
                receiver.onChooseModel(buffer.getInt());
                break;
        }
    }
}
