package uk.ac.imperial.remotesensor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jmedeisis.aglut.Quaternion;
import com.jmedeisis.common.bt.BTConnection;
import com.jmedeisis.common.bt.ControlMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class RemoteActivity extends ActionBarActivity {
    private static final String LOG_TAG = RemoteActivity.class.getSimpleName();
    private static final int REQUEST_ENABLE_BT = 1;
    private final static int REQUEST_SETTINGS = 2;
    private final static int REQUEST_MODEL_IDX = 3;
    public final static String MODEL_IDX_EXTRA = "model_idx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remote);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new SensorFragment())
                    .commit();
        }

        getSupportActionBar().setElevation(0);

        if(null == BluetoothAdapter.getDefaultAdapter()){
            Toast.makeText(this, "Device does not support BT.", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(REQUEST_ENABLE_BT == requestCode){
            if(RESULT_OK == resultCode){
                // all good!
                ((SensorFragment) getSupportFragmentManager().findFragmentById(R.id.container)).huntServer();
            } else {
                Toast.makeText(this, "BT is kinda necessary here...", Toast.LENGTH_SHORT).show();
            }
        } else if(requestCode == REQUEST_SETTINGS){
            ((SensorFragment) getSupportFragmentManager().findFragmentById(R.id.container)).onPreferencesUpdated();
        } else if(requestCode == REQUEST_MODEL_IDX) {
            if (resultCode == RESULT_OK) {
                int idx = data.getIntExtra(MODEL_IDX_EXTRA, -1);
                if(idx >= 0){
                    ((SensorFragment) getSupportFragmentManager().findFragmentById(R.id.container)).onModelSelected(idx);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_remote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_settings: {
                Intent intent = new Intent(RemoteActivity.this, ConfigureSettingsActivity.class);
                startActivityForResult(intent, REQUEST_SETTINGS);
                return true;
            }
            case R.id.action_model: {
                Intent intent = new Intent(RemoteActivity.this, RemoteChooseModelActivity.class);
                startActivityForResult(intent, REQUEST_MODEL_IDX);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        super.onPrepareOptionsMenu(menu);

        MenuItem settingsItem = menu.findItem(R.id.action_settings);
        MenuItem modelItem = menu.findItem(R.id.action_model);
        BTConnection.State state = ((SensorFragment) getSupportFragmentManager().findFragmentById(R.id.container)).latestState;
        if(null == state){
            settingsItem.setVisible(false);
            modelItem.setVisible(false);
        } else {
            switch (state){
                case CONNECTED:
                    settingsItem.setVisible(true);
                    modelItem.setVisible(true);
                    break;
                default:
                    settingsItem.setVisible(false);
                    modelItem.setVisible(false);
                    break;
            }
        }

        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class SensorFragment extends Fragment implements SensorEventListener {

        private BluetoothAdapter btAdapter;
        private BTConnection connection;

        private boolean receiverRegistered = false;

        private boolean transmitOrientation;

        private SensorManager sensorManager;
        private ListView deviceList;
        private TextView textState, textX, textY, textZ;
        private BTConnection.State latestState;
        private List<String> names;
        private List<String> macs;

        private float latestYaw;
        private float latestPitch;
        private float latestRoll;

        // 1 radian = 57.2957795 degrees
        private static final float DEGREES_PER_RADIAN = 57.2957795f;

        private static final int NUM_FILTER_SAMPLES = 15;
        private static final float LOW_PASS_COEFF = 0.5f;
        private final Filter yawFilter;
        private final Filter pitchFilter;
        private final Filter rollFilter;

        private final float[] rotationMatrix = new float[9];
        private final float[] rotationMatrixTemp = new float[9];
        private final float[] rotationMatrixOrigin = new float[9];
        private final float[] orientation = new float[3];
        private final float[] orientationOrigin = new float[3];
        private final float[] latestAccelerations = new float[3];
        private final float[] latestMagFields = new float[3];
        private final float[] rotationQuaternion = new float[4];

        private boolean haveOrigin = false;
        private boolean haveGravData = false;
        private boolean haveAccelData = false;
        private boolean haveMagData = false;

        /** @see {@link android.view.Display#getRotation()}. */
        private int screenRotation;

        /** Determines the basis in which device orientation is measured. */
        public static enum OrientationMode {
            /** Measures absolute yaw / pitch / roll (i.e. relative to the world). */
            ABSOLUTE,
            /**
             * Measures yaw / pitch / roll relative to the starting orientation.
             * The starting orientation is determined upon receiving the first sensor data,
             * but can be manually reset at any time using {@link #resetOrientationOrigin()}.
             */
            RELATIVE
        }
        private static final OrientationMode DEFAULT_ORIENTATION_MODE = OrientationMode.ABSOLUTE;
        private OrientationMode orientationMode = DEFAULT_ORIENTATION_MODE;

        public SensorFragment() {
            names = new ArrayList<>();
            macs = new ArrayList<>();

            yawFilter = new Filter(NUM_FILTER_SAMPLES, LOW_PASS_COEFF, 0);
            pitchFilter = new Filter(NUM_FILTER_SAMPLES, LOW_PASS_COEFF, 0);
            rollFilter = new Filter(NUM_FILTER_SAMPLES, LOW_PASS_COEFF, 0);
        }

        private class ControlGestureListener extends GestureDetector.SimpleOnGestureListener
                implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener,
                ScaleGestureDetector.OnScaleGestureListener {

            @Override
            public boolean onDown(MotionEvent event){
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent event){
                connection.writePackaged(ControlMessage.encodeSetOrigin());
                return true;
            }

            @Override
            public boolean onDoubleTap(MotionEvent event){
                connection.writePackaged(ControlMessage.encodeResetOrigin());
                return true;
            }

            @Override
            public void onLongPress(MotionEvent event){
                connection.writePackaged(ControlMessage.encodeToggleAutoAnimation());
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY){
                if(Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) return false;
                connection.writePackaged(ControlMessage.encodeAdvanceAnimation(1.5f * distanceX / SWIPE_MIN_DISTANCE));
                return true;
            }

            private static final int SWIPE_MIN_DISTANCE = 120;
            private static final int SWIPE_MAX_OFF_PATH = 250;
            private static final int SWIPE_THRESHOLD_VELOCITY = 200;
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY){
                // detect left/right swipes
                if(Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) return false;
                if(Math.abs(velocityX) < SWIPE_THRESHOLD_VELOCITY) return false;

                if(e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE){
                    // swipe to the right
                    connection.writePackaged(ControlMessage.encodeAdvanceAnimation(+1));
                } else if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE){
                    // swipe to the left
                    connection.writePackaged(ControlMessage.encodeAdvanceAnimation(-1));
                }
                return true;
            }

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                connection.writePackaged(ControlMessage.encodeScale(detector.getScaleFactor()));
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return true; // handle this gesture
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {

            }
        }

        public void onPreferencesUpdated(){
            final Context context = getActivity();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if(null != connection){
                final String navControl = context.getString(R.string.pref_magnet_tap_control_navigation);
                connection.writePackaged(ControlMessage.encodePreferences(
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_double_rotation), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_texture), true),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_color_overlay), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_point_cloud_mean), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_side_mean), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_debug_cube), true),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_detach_camera), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_morph), false),
                        sharedPreferences.getBoolean(context.getString(R.string.pref_key_altogether), false),
                        sharedPreferences.getString(context.getString(R.string.pref_key_magnet_tap_control), navControl)
                ));
            }
        }

        public void onModelSelected(int idx){
            if(null != connection){
                connection.writePackaged(ControlMessage.encodeChooseModel(idx));
            }
        }

        public void onNewBTState(BTConnection.State state){
            latestState = state;
            if(null == getView()) return;
            textState.setText("Connection State: " + state);
            switch (state){
                case CONNECTED:
                    getView().findViewById(R.id.device_choice_container).setVisibility(View.GONE);
                    getView().findViewById(R.id.gesture_view).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.gesture_view_decor).setVisibility(View.VISIBLE);

                    getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                    onPreferencesUpdated();
                    break;
                default:
                    getView().findViewById(R.id.device_choice_container).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.gesture_view).setVisibility(View.GONE);
                    getView().findViewById(R.id.gesture_view_decor).setVisibility(View.GONE);

                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    break;
            }

            if(null != getActivity()) ActivityCompat.invalidateOptionsMenu(getActivity());
        }

        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

            screenRotation = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay().getRotation();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_remote, container, false);

            textState = (TextView) rootView.findViewById(R.id.statusText);
            onNewBTState(latestState);
            textX = (TextView) rootView.findViewById(R.id.sensorTextX);
            textY = (TextView) rootView.findViewById(R.id.sensorTextY);
            textZ = (TextView) rootView.findViewById(R.id.sensorTextZ);

            deviceList = (ListView) rootView.findViewById(R.id.deviceList);
            View deviceListEmpty = rootView.findViewById(R.id.deviceListEmpty);
            deviceList.setEmptyView(deviceListEmpty);
            deviceList.setAdapter(deviceAdapter);

            SwitchCompat orientationTransmitSwitch = (SwitchCompat) rootView.findViewById(R.id.orientation_switch);
            transmitOrientation = orientationTransmitSwitch.isChecked();
            orientationTransmitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    transmitOrientation = isChecked;
                }
            });

            rootView.findViewById(R.id.connectButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(LOG_TAG, "selected device pos " + deviceList.getCheckedItemPosition());
                    if(deviceList.getCheckedItemPosition() < 0) return;
                    String MAC = macs.get(deviceList.getCheckedItemPosition());
                    connection.connect(btAdapter.getRemoteDevice(MAC), true);
                }
            });

            final ControlGestureListener gestureListener = new ControlGestureListener();
            final GestureDetector gestureDetector = new GestureDetector(getActivity(), gestureListener);
            final ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(getActivity(), gestureListener);
            rootView.findViewById(R.id.gesture_view).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    scaleGestureDetector.onTouchEvent(event);
                    return gestureDetector.onTouchEvent(event);
                }
            });

            return rootView;
        }

        @Override
        public void onResume(){
            super.onResume();
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                    SensorManager.SENSOR_DELAY_GAME);
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                    SensorManager.SENSOR_DELAY_GAME);
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_GAME);

            btAdapter = BluetoothAdapter.getDefaultAdapter();
            if(!btAdapter.isEnabled()){
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            } else if(null == connection || BTConnection.State.NONE == connection.getState()){
                huntServer();
            }
        }

        @Override
        public void onPause(){
            super.onPause();
            // Unregister this Activity as soon as it's paused to prevent battery drain.
            sensorManager.unregisterListener(this);

            if(receiverRegistered){
                getActivity().unregisterReceiver(broadcastReceiver);
                receiverRegistered = false;
            }
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch(event.sensor.getType()){
                case Sensor.TYPE_GRAVITY:
                    System.arraycopy(event.values, 0, latestAccelerations, 0, 3);
                    haveGravData = true;
                    break;
                case Sensor.TYPE_ACCELEROMETER:
                    if(haveGravData){
                        // gravity sensor data is better! let's not listen to the accelerometer anymore
                        sensorManager.unregisterListener(this,
                                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
                        break;
                    }
                    System.arraycopy(event.values, 0, latestAccelerations, 0, 3);
                    haveAccelData = true;
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    System.arraycopy(event.values, 0, latestMagFields, 0, 3);
                    haveMagData = true;
                    break;
            }

            if(haveDataNecessaryToComputeOrientation()){
                computeOrientation();
            }
        }

        /** @return true if both {@link #latestAccelerations} and {@link #latestMagFields} have valid values. */
        private boolean haveDataNecessaryToComputeOrientation(){
            return (haveGravData || haveAccelData) && haveMagData;
        }

        /**
         * Computes the latest rotation, and stores it in {@link #rotationMatrix}.<p>
         * Should only be called if {@link #haveDataNecessaryToComputeOrientation()} returns true,
         * else result may be undefined.
         * @return true if rotation was retrieved and recalculated, false otherwise.
         */
        private boolean computeRotationMatrix(){
            if(SensorManager.getRotationMatrix(rotationMatrixTemp, null, latestAccelerations, latestMagFields)){
                switch(screenRotation){
                    case Surface.ROTATION_0:
                        SensorManager.remapCoordinateSystem(rotationMatrixTemp,
                                SensorManager.AXIS_X, SensorManager.AXIS_Y, rotationMatrix);
                        break;
                    case Surface.ROTATION_90:
                        SensorManager.remapCoordinateSystem(rotationMatrixTemp,
                                SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, rotationMatrix);
                        break;
                    case Surface.ROTATION_180:
                        SensorManager.remapCoordinateSystem(rotationMatrixTemp,
                                SensorManager.AXIS_MINUS_X, SensorManager.AXIS_MINUS_Y, rotationMatrix);
                        break;
                    case Surface.ROTATION_270:
                        SensorManager.remapCoordinateSystem(rotationMatrixTemp,
                                SensorManager.AXIS_MINUS_Y, SensorManager.AXIS_X, rotationMatrix);
                        break;
                }
                return true;
            }
            return false;
        }

        /**
         * Computes the latest orientation and stores it in {@link #orientation}.
         * Also updates {@link #latestYaw}, {@link #latestPitch} and {@link #latestRoll}
         * as filtered versions of {@link #orientation}.
         */
        private void computeOrientation(){
            if(!transmitOrientation) return;

            synchronized(rotationMatrix){
                if(computeRotationMatrix()){
                    switch(orientationMode){
                        case ABSOLUTE:
                            // get absolute yaw / pitch / roll
                            SensorManager.getOrientation(rotationMatrix, orientation);
                            break;
                        case RELATIVE:
                            if(!haveOrigin){
                                updateOrigin();
                            }

                            // get yaw / pitch / roll relative to original rotation
                            SensorManager.getAngleChange(orientation, rotationMatrix, rotationMatrixOrigin);
                            break;
                    }


                    /* [0] : yaw, rotation around z axis
                     * [1] : pitch, rotation around x axis
                     * [2] : roll, rotation around y axis */
                    final float yaw = orientation[0] * DEGREES_PER_RADIAN;
                    final float pitch = orientation[1] * DEGREES_PER_RADIAN;
                    final float roll = orientation[2] * DEGREES_PER_RADIAN;

                    latestYaw = yawFilter.push(yaw);
                    latestPitch = pitchFilter.push(pitch);
                    latestRoll = rollFilter.push(roll);

                    textX.setText("yaw:\t" + latestYaw);
                    textY.setText("pitch:\t" + latestPitch);
                    textZ.setText("roll:\t" + latestRoll);

                    // transmit quaternion instead
                    Quaternion.getFromRowMajorRotMatrix(rotationQuaternion, rotationMatrix);
                    connection.writePackaged(ControlMessage.encodeOrient(rotationQuaternion[0],
                            rotationQuaternion[1], rotationQuaternion[2], rotationQuaternion[3]));
                }
            }
        }

        /**
         * Manually resets the orientation origin.
         */
        public boolean resetOrientationOrigin(){
            if(haveDataNecessaryToComputeOrientation()){
                synchronized(rotationMatrix){
                    if(computeRotationMatrix()){
                        updateOrigin();
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Resets the internal orientation origin matrix.
         * {@link #computeRotationMatrix()} must have been called prior.
         */
        private void updateOrigin(){
            System.arraycopy(rotationMatrix, 0, rotationMatrixOrigin, 0, 9);
            haveOrigin = true;
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }

        /** Checks for previously bonded devices. */
        private void checkBondedDevices(){
            Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
            for(BluetoothDevice device : pairedDevices){
                if(macs.contains(device.getAddress())) continue;
                Log.d(LOG_TAG, "bonded device name: " + device.getName() + ", address: " + device.getAddress());
                names.add(device.getName());
                macs.add(device.getAddress());
                deviceAdapter.notifyDataSetChanged();
            }
        }

        private void huntServer(){
            connection = new BTConnection();
            onNewBTState(connection.getState());
            connection.addListener(new Messenger(btHandler));

            checkBondedDevices();

            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(broadcastReceiver, filter);
            receiverRegistered = true;
            btAdapter.startDiscovery();
        }

        private final BaseAdapter deviceAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return names.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = View.inflate(getActivity(), android.R.layout.simple_list_item_single_choice, null);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setText(names.get(position) + " @ " + macs.get(position));
                return view;
            }
        };

        private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if(BluetoothDevice.ACTION_FOUND.equals(action)){
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    if(macs.contains(device.getAddress())) return;
                    Log.d(LOG_TAG, "found device name: " + device.getName() + ", address: " + device.getAddress());
                    names.add(device.getName());
                    macs.add(device.getAddress());
                    deviceAdapter.notifyDataSetChanged();
                }
            }
        };

        private final Handler btHandler = new Handler() {
            @Override
            public void handleMessage(Message msg){
                switch(msg.what){
                    case BTConnection.MESSAGE_STATE_CHANGE:
                        BTConnection.State newState = (BTConnection.State) msg.obj;
                        onNewBTState(newState);
                        switch(newState){
                            case NONE:
                            /*connectFragment.statusText.setText("Disconnected.");
                            connectFragment.numRetries++;
                            if(connectFragment.numRetries > MAX_RETRIES){
                                Toast.makeText(connectFragment.getActivity(), "Exceeded maximum retries, try again later.", Toast.LENGTH_LONG).show();
                                connectFragment.getActivity().getApplicationContext().stopService(new Intent(connectFragment.getActivity(), BTConnectionService.class));
                                connectFragment.getActivity().finish();
                            } else {
                                Toast.makeText(connectFragment.getActivity(), "Failed to connect. Retrying, try " + connectFragment.numRetries + " out of " + MAX_RETRIES, Toast.LENGTH_SHORT).show();
                                connectFragment.connectToDevice(MAC_HCARD);
                            }*/
                                break;
                            case CONNECTING:
                            /*connectFragment.statusText.setText("Connecting...");
                            Log.d(LOG_TAG, "Service tells me it's connecting..");*/
                                break;
                            case CONNECTED:
                            /*connectFragment.statusText.setText("Connected!");
                            // hurrah, move forward!
                            connectFragment.onConnected();*/
                                break;
                        }
                        break;
                }
            }
        };

        /** Ring buffer low-pass filter. */
        private class Filter {
            float[] buffer;
            float sum;
            int lastIndex;
            /** 0-1*/
            float factor;

            public Filter(int samples, float factor, float initialValue){
                buffer = new float[samples];
                this.factor = factor;
                lastIndex = 0;
                reset(initialValue);
            }

            public void reset(float value){
                sum = value * buffer.length;
                for(int i = 0; i < buffer.length; i++){
                    buffer[i] = value;
                }
            }

            /**
             * Pushes new sample to filter.
             * @return new smoothed value.
             */
            public float push(float value){
                // do low-pass
                value = buffer[lastIndex] + factor * (value - buffer[lastIndex]);

                // subtract oldest sample
                sum -= buffer[lastIndex];
                // add new sample
                sum += value;
                buffer[lastIndex] = value;

                // advance index
                lastIndex = lastIndex >= buffer.length - 1? 0 : lastIndex + 1;

                return get();
            }

            /** @return smoothed value. */
            public float get(){
                return sum / buffer.length;
            }
        }
    }
}
