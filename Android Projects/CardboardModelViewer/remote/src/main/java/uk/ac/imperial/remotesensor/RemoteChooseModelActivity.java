package uk.ac.imperial.remotesensor;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jmedeisis.common.model.ModelData;


public class RemoteChooseModelActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_choose_model);

        ListView listView = (ListView) findViewById(R.id.model_list);
        listView.setAdapter(new ModelAdapter());
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Intent data = new Intent();
                data.putExtra(RemoteActivity.MODEL_IDX_EXTRA, position);
                setResult(RESULT_OK, data);
                finish();
            }
        });
    }

    private class ModelAdapter extends BaseAdapter {

        private final ModelData[] models;

        public ModelAdapter(){
            models = ModelData.values();
        }

        @Override
        public int getCount() {
            return models.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = View.inflate(parent.getContext(), R.layout.item_model, null);

            TextView text = (TextView) view.findViewById(R.id.label);
            TextView caption = (TextView) view.findViewById(R.id.caption);

            text.setText(models[position].label);

            caption.setVisibility(View.VISIBLE);
            StringBuilder captionText = new StringBuilder();
            for(int i = 0; i < models[position].components.length; i++){
                captionText.append(models[position].components[i].label + ", ");
            }
            captionText.delete(captionText.length()-2, captionText.length()); // trim last comma
            caption.setText(captionText);
            if(captionText.length() == 0) caption.setVisibility(View.GONE);

            return view;
        }
    }


}
