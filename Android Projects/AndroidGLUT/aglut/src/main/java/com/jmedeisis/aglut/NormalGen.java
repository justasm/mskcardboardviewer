package com.jmedeisis.aglut;

import android.util.FloatMath;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Justas on 16/10/2014.
 */
public class NormalGen {
    private static final int BYTES_PER_FLOAT = 4;

    /**
     * Calculates surface normals for indexed geometry. Assumes vertices is non-packed.
     * https://www.opengl.org/wiki/Calculating_a_Surface_Normal
     * Assumes geometry is triangles.
     */
    public static FloatBuffer getNormals(FloatBuffer vertices, ShortBuffer indices){
        // # normal floats = # indices x 3 floats per normal x bytes per float
        final int numNormalFloats = indices.capacity() * 3;
        ByteBuffer bbNormals = ByteBuffer.allocateDirect(numNormalFloats * BYTES_PER_FLOAT);
        bbNormals.order(ByteOrder.nativeOrder());
        FloatBuffer normals = bbNormals.asFloatBuffer();
        normals.put(new float[numNormalFloats]); // TODO is this necessary? dunno if buffer is zeroed on allocation..
        normals.position(0);

        // iterate over each face, calculate its normal, and store as running sum per member vertex
        indices.position(0);
        while(indices.hasRemaining()){
            short p1i = indices.get();
            short p2i = indices.get();
            short p3i = indices.get();

            // we're assuming vertices is contiguous positions only here
            float p1x = vertices.get(3 * p1i + 0);
            float p1y = vertices.get(3 * p1i + 1);
            float p1z = vertices.get(3 * p1i + 2);

            float p2x = vertices.get(3 * p2i + 0);
            float p2y = vertices.get(3 * p2i + 1);
            float p2z = vertices.get(3 * p2i + 2);

            float p3x = vertices.get(3 * p3i + 0);
            float p3y = vertices.get(3 * p3i + 1);
            float p3z = vertices.get(3 * p3i + 2);

            float ux = p2x - p1x;
            float uy = p2y - p1y;
            float uz = p2z - p1z;

            float vx = p3x - p1x;
            float vy = p3y - p1y;
            float vz = p3z - p1z;

            float normalX = uy * vz - uz * vy;
            float normalY = uz * vx - ux * vz;
            float normalZ = ux * vy - uy * vx;

            // normalise it
            float length = FloatMath.sqrt(normalX * normalX + normalY * normalY + normalZ * normalZ);

            float nnx = normalX / length;
            float nny = normalY / length;
            float nnz = normalZ / length;

            normals.put(3 * p1i + 0, normals.get(3 * p1i + 0) + nnx);
            normals.put(3 * p1i + 1, normals.get(3 * p1i + 1) + nny);
            normals.put(3 * p1i + 2, normals.get(3 * p1i + 2) + nnz);

            normals.put(3 * p2i + 0, normals.get(3 * p2i + 0) + nnx);
            normals.put(3 * p2i + 1, normals.get(3 * p2i + 1) + nny);
            normals.put(3 * p2i + 2, normals.get(3 * p2i + 2) + nnz);

            normals.put(3 * p3i + 0, normals.get(3 * p3i + 0) + nnx);
            normals.put(3 * p3i + 1, normals.get(3 * p3i + 1) + nny);
            normals.put(3 * p3i + 2, normals.get(3 * p3i + 2) + nnz);
        }

        // normalise all the summed normals per vertex
        indices.position(0);
        while(indices.hasRemaining()){
            short i = indices.get();

            float nx = normals.get(3 * i + 0);
            float ny = normals.get(3 * i + 1);
            float nz = normals.get(3 * i + 2);

            // normalise it
            float length = FloatMath.sqrt(nx * nx + ny * ny + nz * nz);

            float nnx = nx / length;
            float nny = ny / length;
            float nnz = nz / length;

            normals.put(3 * i + 0, nnx);
            normals.put(3 * i + 1, nny);
            normals.put(3 * i + 2, nnz);
        }

        indices.position(0);
        normals.position(0);
        return normals;
    }
}
