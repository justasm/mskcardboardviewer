package com.jmedeisis.aglut;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * A generic OpenGL program containing linked vertex and fragment shaders.
 * Justas 2013/2014
 */
public class Program {
	private static final String LOG_TAG = "Program";
	private static final boolean DEBUG = true;
	private static final int NO_HANDLE = 0;
	
	public static final String A_POSCOORD = "a_Position";
	public static final String A_COLOR = "a_Color";
	public static final String A_TEXCOORD = "a_TexCoord";
	
	public static final String U_TEXTURE = "u_Texture";
	public static final String U_MVPMATRIX = "u_MVPMatrix";
	public static final String U_COLOR = "u_Color";
	
	private int handle = NO_HANDLE;
	private Map<String, Integer> handles;
	
	private final int vertexShaderResId;
	private final int fragmentShaderResId;
	
	public Program(Context context, final int vertexShaderResId, final int fragmentShaderResId){
		this.vertexShaderResId = vertexShaderResId;
		this.fragmentShaderResId = fragmentShaderResId;
		reload(context);
	}
	
	public void use(){
		GLES20.glUseProgram(handle);
	}
	
	/**
     * Deletes the underlying GL program object.
     * List of attributes, uniforms, as well as the ids of the shaders are not modified.
     */
	public void recycle(){
		if(NO_HANDLE != handle){
			GLES20.glDeleteProgram(handle);
			handle = NO_HANDLE;
		}
	}
	
	/** Recreates and re-uploads program. Designed for use after a GL context loss. */
	public void reload(Context context){
		//define shaders
		final String vertexShader;
		final String fragmentShader;
		try {
			vertexShader = readRawTextFile(context, vertexShaderResId);
			fragmentShader = readRawTextFile(context, fragmentShaderResId);
		} catch (IOException e) {
			Log.e(LOG_TAG, "Failed to load shader strings.");
			return;
		}
		final int vertexShaderHandle = compileShader(GLES20.GL_VERTEX_SHADER, vertexShader);
		final int fragmentShaderHandle = compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
		
		//create program
		handle = createAndLinkProgram(vertexShaderHandle, fragmentShaderHandle);
		
		if(NO_HANDLE == handle){
			if(DEBUG) Log.e(LOG_TAG, "Failed to link program " + this + " handle " + handle);
			return;
		}
		if(DEBUG) Log.d(LOG_TAG, "Successfully linked program " + this + " handle " + handle);
		
		//retrieve handles for attributes and uniforms
		int[] numAttributes = new int[1];
		GLES20.glGetProgramiv(handle, GLES20.GL_ACTIVE_ATTRIBUTES, numAttributes, 0);
		int[] numUniforms = new int[1];
		GLES20.glGetProgramiv(handle, GLES20.GL_ACTIVE_UNIFORMS, numUniforms, 0);
		
		if(DEBUG){
			Log.d(LOG_TAG, "Number attributes: " + numAttributes[0]);
			Log.d(LOG_TAG, "Number uniforms: " + numUniforms[0]);
		}
		
		handles = new HashMap<String, Integer>(numAttributes[0] + numUniforms[0]);
		
		try {
			final int MAX_NAME_LENGTH = 64;
			byte[] rawName = new byte[MAX_NAME_LENGTH];
			int[] nameLength = new int[1];
			int[] dummy = new int[0];
			for(int i = 0; i < numAttributes[0]; i++){
				GLES20.glGetActiveAttrib(handle, i, MAX_NAME_LENGTH, nameLength, 0, dummy, 0, dummy, 0, rawName, 0);
				String attrName = new String(rawName, 0, nameLength[0], "UTF-8");
				if(DEBUG) Log.d(LOG_TAG, "Found program attribute " + i + " named " + attrName + " of length " + nameLength[0]);
				handles.put(attrName, i);
			}
			for(int i = 0; i < numUniforms[0]; i++){
				GLES20.glGetActiveUniform(handle, i, MAX_NAME_LENGTH, nameLength, 0, dummy, 0, dummy, 0, rawName, 0);
				String uniName = new String(rawName, 0, nameLength[0], "UTF-8");
				if(DEBUG) Log.d(LOG_TAG, "Found program uniform " + i + " named " + uniName + " of length " + nameLength[0]);
				handles.put(uniName, i);
			}
		} catch (UnsupportedEncodingException e) {
			Log.e(LOG_TAG, "Charset UTF-8 unsupported..");
			e.printStackTrace();
			throw new RuntimeException();
		}
	}
	
	/** @return a handle to a program variable by the given name, or -1 if such does not exist. */
	public int getHandle(final String name){
		if(!handles.containsKey(name)) return -1;
        return handles.get(name);
	}

    /**
     * Creates an OpenGL shader from the provided parameters.
     * @return shader handle.
     */
    private static int compileShader(final int shaderType, final String shaderSource){
        //create shader object
        int handle = GLES20.glCreateShader(shaderType);

        if(0 != handle){
            //pass shader source
            GLES20.glShaderSource(handle, shaderSource);
            //compile
            GLES20.glCompileShader(handle);
            //get compilation status
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(handle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            //if compilation failed, delete shader
            if(0 == compileStatus[0]){
                Log.e(LOG_TAG, "Error compiling shader: " + GLES20.glGetShaderInfoLog(handle));
                GLES20.glDeleteShader(handle);
                handle = 0;
            }
        }
        if(0 == handle){
            throw new RuntimeException("Error creating shader.");
        }

        return handle;
    }

    /**
     * Creates an OpenGL program, binding and linking the provided shaders.
     * @return program handle.
     */
    private static int createAndLinkProgram(final int vertexShaderHandle, final int fragmentShaderHandle){
        //create program object
        int programHandle = GLES20.glCreateProgram();

        if(0 != programHandle){
            //bind vertex shader to program
            GLES20.glAttachShader(programHandle, vertexShaderHandle);

            //bind fragment shader to program
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);

            //bind attributes
            //not actually necessary for my purposes, but note -
            //http://stackoverflow.com/questions/6898146/use-of-glbindattriblocation-function-in-opengl-es
            //http://stackoverflow.com/questions/12051205/why-should-i-use-glbindattriblocation
            //could potentially add library-wide standard attribute locations, but this is currently not the case
			/*if(null != attributes){
				for(int i = 0; i < attributes.length; i++){
					GLES20.glBindAttribLocation(programHandle, i, attributes[i]);
				}
			}*/

            //link the shaders into program
            GLES20.glLinkProgram(programHandle);

            //get link status
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

            //if linking failed, delete program
            if(0 == linkStatus[0]){
                Log.e(LOG_TAG, "Error linking program: " + GLES20.glGetProgramInfoLog(programHandle));
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
            }

            //detach and delete shaders - they are no longer necessary
            GLES20.glDetachShader(programHandle, vertexShaderHandle);
            GLES20.glDetachShader(programHandle, fragmentShaderHandle);

            GLES20.glDeleteShader(vertexShaderHandle);
            GLES20.glDeleteShader(fragmentShaderHandle);
        } else {
            throw new RuntimeException("Error creating program.");
        }

        return programHandle;
    }

    /**
     * Reads a raw text file and returns as string.
     * @param context application context.
     * @param resId unique resource id.
     * @return string representation.
     * @throws java.io.IOException if text file could not be read.
     */
    public static String readRawTextFile(final Context context, final int resId) throws IOException{
        InputStream inputStream = context.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);

        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null){
                text.append(line);
                text.append('\n');
            }
        } catch (IOException ioe) {
            Log.e(LOG_TAG, "Problem reading txt file.");
            throw ioe;
        }

        return text.toString();
    }
}
