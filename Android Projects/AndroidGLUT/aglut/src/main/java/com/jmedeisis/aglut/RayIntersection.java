package com.jmedeisis.aglut;

import static com.jmedeisis.aglut.FloatArrayVector3.add;
import static com.jmedeisis.aglut.FloatArrayVector3.cross;
import static com.jmedeisis.aglut.FloatArrayVector3.dot;
import static com.jmedeisis.aglut.FloatArrayVector3.mul;
import static com.jmedeisis.aglut.FloatArrayVector3.sub;

/**
 * Created by Justas on 03/12/2014.
 */
public class RayIntersection {

    /**
     * Note that rayStart, rayEnd and the triangle must be in the same space.
     * http://www.graphics.cornell.edu/pubs/1997/MT97.pdf
     */
    private final static double EPSILON = 0.000001;
    private static float[] rayDir = new float[3];
    private static float[] edge1 = new float[3];
    private static float[] edge2 = new float[3];
    private static float[] pvec = new float[3];
    private static float[] tvec = new float[3];
    private static float[] qvec = new float[3];
    private static float[] rayLength = new float[3];
    public static boolean rayVsTriangle(float[] rayStart, float[] rayEnd, float[] t0, float[] t1, float[] t2, float[] hit){
        sub(rayDir, rayEnd, rayStart);

        // vectors for two edges sharing t0
        sub(edge1, t1, t0);
        sub(edge2, t2, t0);

        // begin calculating determinant; also used to calculate u parameter
        cross(pvec, rayDir, edge2);

        // if determinant is near zero, ray lies in plane of triangle
        float det = dot(edge1, pvec);

        if(det < EPSILON) return false;

        // calculate distance from t0 to ray origin
        sub(tvec, rayStart, t0);

        // calculate u parameter and test bounds
        float u = dot(tvec, pvec);
        if(u < 0.0 || u > det) return false;

        // prepare to test v parameter
        cross(qvec, tvec, edge1);

        // calculate v parameter and test bounds
        float v = dot(rayDir, qvec);
        if(v < 0.0 || u + v > det) return false;

        // calculate t, scale parameters, ray intersects triangle
        float t = dot(edge2, qvec);
        float invDet = 1.0f / det;
        t *= invDet;
//        u *= invDet;
//        v *= invDet;
        // don't need 'em right now..

        mul(rayLength, rayDir, t);
        add(hit, rayStart, rayLength);
        return true;
    }
}
