package com.jmedeisis.aglut;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A wrapper for an OpenGL texture object.
 * Justas 2013-2015
 */
public class Texture {
	private static final String LOG_TAG = "Texture";
	private static final int NO_HANDLE = 0;
	public static final BitmapFactory.Options NO_PRESCALE_OPTIONS;
	
	static{
		//disable prescaling
		NO_PRESCALE_OPTIONS = new BitmapFactory.Options();
		NO_PRESCALE_OPTIONS.inScaled = false;
	}
	
	/** The bitmap from which this texture was created.
	 * Reference kept for reloading upon GL context loss. */
	private Bitmap bitmap;
	private int handle = NO_HANDLE;
	private int width, height;
	
	/**
	 * Constructs a texture from a bitmap pointed at by the given File.
	 */
	public Texture(final File location){
		try {
			set(BitmapFactory.decodeStream(new FileInputStream(location), null, NO_PRESCALE_OPTIONS));
		} catch (FileNotFoundException e) {
			Log.e(LOG_TAG, "Failed to locate bitmap at location " + location, e);
		}
	}
	
	/**
	 * Constructs a texture from a bitmap located in assets,
	 * whose location is identified by the provided path.
	 */
	public Texture(final Context context, final String path) {
		try {
			set(BitmapFactory.decodeStream(context.getAssets().open(path), null, NO_PRESCALE_OPTIONS));
		} catch (IOException e) {
			Log.e(LOG_TAG, "Failed to decode bitmap from stream at asset path " + path, e);
		}
	}
	
	/**
	 * Constructs a texture from a bitmap identified by the provided resource id.
	 */
	public Texture(final Context context, final int resourceId){
		set(BitmapFactory.decodeResource(context.getResources(), resourceId, NO_PRESCALE_OPTIONS));
	}
	
	/**
	 * Constructs a texture from the provided bitmap.
	 */
	public Texture(final Bitmap bitmap){
		set(bitmap);
	}
	
	/**
	 * Replaces the backing GL texture with a new bitmap.
	 */
	public void set(final Bitmap bitmap){
		if(handle != NO_HANDLE){
			recycleHandle(handle);
		}
		handle = genHandle();
		
		//bind texture
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);
		
		//set filtering
		//for draws smaller than original texture
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		//for draws bigger than original texture
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		
		//load bitmap into bound texture
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
		
		//store info about texture
		width = bitmap.getWidth();
		height = bitmap.getHeight();
		
		//recycle used bitmap
		//bitmap.recycle();
		//store a reference to the bitmap used
		this.bitmap = bitmap;
	}
	
	/** A wrap mode for a texture. */
	public static enum TextureWrap{
		/** Texture coordinates past the bounds take the value of the farthest coordinate in that direction (GL_CLAMP_TO_EDGE). */
		CLAMP_TO_EDGE(GLES20.GL_CLAMP_TO_EDGE),
		/** Texture coordinates past the bounds loop around and the texture repeats (GL_REPEAT). */
		REPEAT(GLES20.GL_REPEAT),
		/** Texture coordinates past the bounds loop around in a mirrored fashion (GL_MIRRORED_REPEAT).*/
		MIRRORED_REPEAT(GLES20.GL_MIRRORED_REPEAT);
		
		private final int glInt;
		private TextureWrap(int glInt){
			this.glInt = glInt;
		}
	}
	/** 
	 * Sets the wrap for this texture. The default is {@link com.jmedeisis.aglut.Texture.TextureWrap#CLAMP_TO_EDGE}.
	 * @see com.jmedeisis.aglut.Texture.TextureWrap
	 */
	public void setWrap(TextureWrap wrapS, TextureWrap wrapT){
		if(handle == NO_HANDLE) return;
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, wrapS.glInt);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, wrapT.glInt);
		//unbind texture
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}

    /** Generates a set of textures at different sizes for optimal rendering. */
    public void enableMipmaps(){
        if(handle == NO_HANDLE) return;
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);
        GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    /** A filter mode for rendering texels to pixels. */
    public static enum TextureFilter{
        NEAREST(GLES20.GL_NEAREST),
        LINEAR(GLES20.GL_LINEAR);

        private final int glInt;
        private TextureFilter(int glInt){
            this.glInt = glInt;
        }
    }

    /** Configure the filtering to use when the texture is drawn smaller / larger than its original size. */
    public void setFiltering(TextureFilter minFilter, TextureFilter maxFilter){
        if(handle == NO_HANDLE) return;
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);
        //for draws smaller than original texture
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, minFilter.glInt);
        //for draws bigger than original texture
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, maxFilter.glInt);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
	
	/** @return the width of this texture in pixels. */
	public int width(){
		return width;
	}
	
	/** @return the height of this texture in pixels. */
	public int height(){
		return height;
	}
	
	/** @return this texture's GL handle. */
	public int handle(){
		return handle;
	}
	
	/**
	 * Binds this texture to the given texture unit.
	 * @param textureUnit appended to {@link android.opengl.GLES20#GL_TEXTURE0} to get the texture unit id.
	 */
	public void bind(int textureUnit){
		//set active texture unit
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + textureUnit);
		//bind the texture
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);
	}
	
	private static int genHandle(){
		final int[] textureHandle = new int[1];
		//get unique global identifier
		GLES20.glGenTextures(1, textureHandle, 0);
		
		if(textureHandle[0] == 0){
			Log.e(LOG_TAG, "Error creating texture handle.");
		}
		
		return textureHandle[0];
	}
	
	private static void recycleHandle(int handle){
		final int[] textureHandle = new int[1];
		textureHandle[0] = handle;
		
		GLES20.glDeleteTextures(1, textureHandle, 0);
	}
}
