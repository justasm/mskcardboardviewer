package com.jmedeisis.aglut;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Justas on 30/10/2014.
 */
public class ColorGen {
    private static final int BYTES_PER_FLOAT = 4;

    /**
     * Returns a FloatBuffer with the given colour.
     */
    public static FloatBuffer getColors(int vertexCount, float r, float g, float b, float a){
        ByteBuffer bbColors = ByteBuffer.allocateDirect(vertexCount * 4 * BYTES_PER_FLOAT);
        bbColors.order(ByteOrder.nativeOrder());
        FloatBuffer colors = bbColors.asFloatBuffer();
        colors.position(0);

        for(int i = 0; i < vertexCount; i++){
            colors.put(new float[]{r, g, b, a});
        }

        colors.position(0);
        return colors;
    }
}
