package com.jmedeisis.aglut;

import android.opengl.Matrix;

/**
 * Created by Justas on 29/10/2014.
 */
public class GLMatrix {

    public static void multiplyMM(float[] result, float[] lhs, float[] rhs){
        Matrix.multiplyMM(result, 0, lhs, 0, rhs, 0);
    }

    public static void multiplyMV(float[] resultVec, float[] lhsMat, float[] rhsVec){
        Matrix.multiplyMV(resultVec, 0, lhsMat, 0, rhsVec, 0);
    }

    public static void setIdentity(float[] mat){
        Matrix.setIdentityM(mat, 0);
    }

    public static void cpy(float[] result, float[] input){
        System.arraycopy(input, 0, result, 0, input.length);
    }
}
