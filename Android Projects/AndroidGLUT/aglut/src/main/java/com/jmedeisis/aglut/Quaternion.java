/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.jmedeisis.aglut;

/**
 * Created by Justas on 09/12/2014.
 *
 * Expext row-major input.
 * <pre>
 *    /  M[ 0]   M[ 1]   M[ 2]  \
 *    |  M[ 3]   M[ 4]   M[ 5]  |
 *    \  M[ 6]   M[ 7]   M[ 8]  /
 * </pre>
 *
 * Some methods used originate from the open-source LibGDX library.
 * @author badlogicgames@gmail.com
 * @author vesuvio
 * @author xoppa
 */
public class Quaternion {
    /**
     * Sets the Quaternion from the given row-major rotation matrix.
     * @param out [x, y, z, w]
     */
    public static void getFromRowMajorRotMatrix(float[] out, float[] matrix) {
        getFromAxes(out, false, matrix[0], matrix[1], matrix[2],
                matrix[3], matrix[4], matrix[5], matrix[6], matrix[7], matrix[8]);
    }

    /**
     * <p>
     * Sets the Quaternion from the given x-, y- and z-axis.
     * </p>
     *
     * <p>
     * Taken from Bones framework for JPCT, see http://www.aptalkarga.com/bones/ which in turn took it from Graphics Gem code at
     * ftp://ftp.cis.upenn.edu/pub/graphics/shoemake/quatut.ps.Z.
     * </p>
     *
     * @param normalizeAxes whether to normalize the axes (necessary when they contain scaling)
     * @param xx x-axis x-coordinate
     * @param xy x-axis y-coordinate
     * @param xz x-axis z-coordinate
     * @param yx y-axis x-coordinate
     * @param yy y-axis y-coordinate
     * @param yz y-axis z-coordinate
     * @param zx z-axis x-coordinate
     * @param zy z-axis y-coordinate
     * @param zz z-axis z-coordinate
     * @param out [x, y, z, w]
     */
    public static void getFromAxes(float[] out, boolean normalizeAxes,
                            float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz) {
        float x, y, z, w;
        if (normalizeAxes) {
            final float lx = 1f / vec3Length(xx, xy, xz);
            final float ly = 1f / vec3Length(yx, yy, yz);
            final float lz = 1f / vec3Length(zx, zy, zz);
            xx *= lx;
            xy *= lx;
            xz *= lx;
            yz *= ly;
            yy *= ly;
            yz *= ly;
            zx *= lz;
            zy *= lz;
            zz *= lz;
        }
        // the trace is the sum of the diagonal elements; see
        // http://mathworld.wolfram.com/MatrixTrace.html
        final float t = xx + yy + zz;

        // we protect the division by s by ensuring that s>=1
        if (t >= 0) { // |w| >= .5
            float s = (float)Math.sqrt(t + 1); // |s|>=1 ...
            w = 0.5f * s;
            s = 0.5f / s; // so this division isn't bad
            x = (zy - yz) * s;
            y = (xz - zx) * s;
            z = (yx - xy) * s;
        } else if ((xx > yy) && (xx > zz)) {
            float s = (float)Math.sqrt(1.0 + xx - yy - zz); // |s|>=1
            x = s * 0.5f; // |x| >= .5
            s = 0.5f / s;
            y = (yx + xy) * s;
            z = (xz + zx) * s;
            w = (zy - yz) * s;
        } else if (yy > zz) {
            float s = (float)Math.sqrt(1.0 + yy - xx - zz); // |s|>=1
            y = s * 0.5f; // |y| >= .5
            s = 0.5f / s;
            x = (yx + xy) * s;
            z = (zy + yz) * s;
            w = (xz - zx) * s;
        } else {
            float s = (float)Math.sqrt(1.0 + zz - xx - yy); // |s|>=1
            z = s * 0.5f; // |z| >= .5
            s = 0.5f / s;
            x = (xz + zx) * s;
            y = (zy + yz) * s;
            w = (yx - xy) * s;
        }

        out[0] = x;
        out[1] = y;
        out[2] = z;
        out[3] = w;
    }

    private static float vec3Length(float x, float y, float z){
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    /** Get the pole of the gimbal lock, if any.
     * @param in [x, y, z, w]
     * @return positive (+1) for north pole, negative (-1) for south pole, zero (0) when no gimbal lock */
    public static int getGimbalPole(float[] in) {
        final float t = in[1]*in[0] + in[2]*in[3];
        return t > 0.499f ? 1 : (t < -0.499f ? -1 : 0);
    }

    /** Get the roll euler angle in radians, which is the rotation around the z axis. Requires that this quaternion is normalized.
     * @return the rotation around the z axis in radians (between -PI and +PI) */
    public static float getRollRad(float[] in) {
        final float x = in[0];
        final float y = in[1];
        final float z = in[2];
        final float w = in[3];
        final int pole = getGimbalPole(in);
        return pole == 0 ? (float) Math.atan2(2f*(w*z + y*x), 1f - 2f * (x*x + z*z)) : (float)pole * 2f * (float)Math.atan2(y, w);
    }

    /** Get the pitch euler angle in radians, which is the rotation around the x axis. Requires that this quaternion is normalized.
     * @return the rotation around the x axis in radians (between -(PI/2) and +(PI/2)) */
    public static float getPitchRad(float[] in) {
        final float x = in[0];
        final float y = in[1];
        final float z = in[2];
        final float w = in[3];
        final int pole = getGimbalPole(in);
        return pole == 0 ? (float)Math.asin(clamp(2f*(w*x-z*y), -1f, 1f)) : (float)pole * (float)Math.PI * 0.5f;
    }

    /** Get the yaw euler angle in radians, which is the rotation around the y axis. Requires that this quaternion is normalized.
     * @return the rotation around the y axis in radians (between -PI and +PI) */
    public static float getYawRad(float[] in) {
        final float x = in[0];
        final float y = in[1];
        final float z = in[2];
        final float w = in[3];
        return getGimbalPole(in) == 0 ? (float)Math.atan2(2f*(y*w + x*z), 1f - 2f*(y*y+x*x)) : 0f;
    }

    private static float clamp(float value, float min, float max){
        if(value < min) return min;
        if(value > max) return max;
        return value;
    }

    /** Sets the quaternion to the given euler angles in radians.
     * @param yaw the rotation around the y axis in radians
     * @param pitch the rotation around the x axis in radians
     * @param roll the rotation around the z axis in radians
     * @return this quaternion */
    /*public static void getEulerAnglesRad (float[] out, float yaw, float pitch, float roll) {
        final float hr = roll * 0.5f;
        final float shr = (float)Math.sin(hr);
        final float chr = (float)Math.cos(hr);
        final float hp = pitch * 0.5f;
        final float shp = (float)Math.sin(hp);
        final float chp = (float)Math.cos(hp);
        final float hy = yaw * 0.5f;
        final float shy = (float)Math.sin(hy);
        final float chy = (float)Math.cos(hy);
        final float chy_shp = chy * shp;
        final float shy_chp = shy * chp;
        final float chy_chp = chy * chp;
        final float shy_shp = shy * shp;

        x = (chy_shp * chr) + (shy_chp * shr); // cos(yaw/2) * sin(pitch/2) * cos(roll/2) + sin(yaw/2) * cos(pitch/2) * sin(roll/2)
        y = (shy_chp * chr) - (chy_shp * shr); // sin(yaw/2) * cos(pitch/2) * cos(roll/2) - cos(yaw/2) * sin(pitch/2) * sin(roll/2)
        z = (chy_chp * shr) - (shy_shp * chr); // cos(yaw/2) * cos(pitch/2) * sin(roll/2) - sin(yaw/2) * sin(pitch/2) * cos(roll/2)
        w = (chy_chp * chr) + (shy_shp * shr); // cos(yaw/2) * cos(pitch/2) * cos(roll/2) + sin(yaw/2) * sin(pitch/2) * sin(roll/2)
        return this;
    }*/

    /** Spherical linear interpolation between this quaternion and the other quaternion, based on the alpha value in the range
     * [0,1]. Taken from. Taken from Bones framework for JPCT, see http://www.aptalkarga.com/bones/
     * @param end the end quaternion
     * @param alpha alpha in the range [0,1] */
    public static void slerp (float[] out, float[] start, float[] end, float alpha) {
        final float dot = start[0] * end[0] + start[1] * end[1] + start[2] * end[2] + start[3] * end[3];
        float absDot = dot < 0.f ? -dot : dot;

        // Set the first and second scale for the interpolation
        float scale0 = 1 - alpha;
        float scale1 = alpha;

        // Check if the angle between the 2 quaternions was big enough to
        // warrant such calculations
        if ((1 - absDot) > 0.1) {// Get the angle between the 2 quaternions,
            // and then store the sin() of that angle
            final double angle = Math.acos(absDot);
            final double invSinTheta = 1f / Math.sin(angle);

            // Calculate the scale for q1 and q2, according to the angle and
            // it's sine value
            scale0 = (float)(Math.sin((1 - alpha) * angle) * invSinTheta);
            scale1 = (float)(Math.sin((alpha * angle)) * invSinTheta);
        }

        if (dot < 0.f) scale1 = -scale1;

        // Calculate the x, y, z and w values for the quaternion by using a
        // special form of linear interpolation for quaternions.
        out[0] = (scale0 * start[0]) + (scale1 * end[0]); // x
        out[1] = (scale0 * start[1]) + (scale1 * end[1]); // y
        out[2] = (scale0 * start[2]) + (scale1 * end[2]); // z
        out[3] = (scale0 * start[3]) + (scale1 * end[3]); // w
    }


    public static void power(float[] out, float[] in, float n){
        float r = FloatArrayVector3.length(in);
        float t = r > 0.00001f ? (float)Math.atan2(r, in[3])/r: 0.f;

        out[0] = in[0] * t * n;
        out[1] = in[1] * t * n;
        out[2] = in[2] * t * n;
        out[3] = 0.5f * (float) Math.log(in[3]*in[3] + FloatArrayVector3.length2(in)) * n;


        // exp
        r = FloatArrayVector3.length(out);
        float et = (float) Math.exp(out[3]);
        float s = r >= 0.00001f ? et*(float)Math.sin(r)/r: 0f;

        out[0] = out[0] * s;
        out[1] = out[1] * s;
        out[2] = out[2] * s;
        out[3] = et * (float) Math.cos(r);
    }
}
