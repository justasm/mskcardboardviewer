package com.jmedeisis.aglut;

/**
 * Utilities for 3-element float arrays representing vectors.
 *
 * Created by Justas on 03/12/2014.
 */
public class FloatArrayVector3 {

    public static void set(float[] res, float[] u){
        res[0] = u[0];
        res[1] = u[1];
        res[2] = u[2];
    }

    public static void set(float[] res, float x, float y, float z){
        res[0] = x;
        res[1] = y;
        res[2] = z;
    }

    public static void add(float[] res, float[] u, float[] v){
        res[0] = u[0] + v[0];
        res[1] = u[1] + v[1];
        res[2] = u[2] + v[2];
    }

    public static void mul(float[] res, float[] u, float a){
        res[0] = u[0] * a;
        res[1] = u[1] * a;
        res[2] = u[2] * a;
    }

    public static void sub(float[] res, float[] u, float[] v){
        res[0] = u[0] - v[0];
        res[1] = u[1] - v[1];
        res[2] = u[2] - v[2];
    }

    /** Dot (inner) product. */
    public static float dot(float[] u, float[] v){
        return u[0] * v[0] + u[1] * v[1] + u[2] * v[2];
    }

    /** Cross (outer) product. */
    public static void cross(float[] res, float[] u, float[] v){
        res[0] = u[1] * v[2] - u[2] * v[1];
        res[1] = u[2] * v[0] - u[0] * v[2];
        res[2] = u[0] * v[1] - u[1] * v[0];
    }

    public static float length2(float[] u){
        return dot(u, u);
    }

    /** Consider using quicker {@link #length2(float[])} when just using for comparison. */
    public static float length(float[] u){
        return (float) Math.sqrt(dot(u, u));
    }

    public static void normalize(float[] u){
        final float length = length(u);
        u[0] = u[0] / length;
        u[1] = u[1] / length;
        u[2] = u[2] / length;
    }
}
